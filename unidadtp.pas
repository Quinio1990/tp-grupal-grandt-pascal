unit    unidadtp;

INTERFACE

uses    AyPDT,crt;

const   MAXPRES=60000000;
        MAXJUGEQUI=15;
        MAXUSUAR=8;
        FECHAS=19;
        JUGTOTAL=623;
        MAXJUGMISMOEQUIPO=3;
        MAXCAMBIOS=4;
        CANTEQUIREAL=20;
        opcvalida=4;
        Njugadores=600;
        leer40=40;
        tamaniopantalla=22;


type    trapuestas= record
                        porfecha:byte;
                        oro:integer;
                        plata:byte;
                        bronce:byte;
                    end;
{Este registro sirve para guardar la configuracion de apuestas elegidas para todo el torneo
En porfecha se guarda el premio por ganara la fecha
En oro se guarda el premio por ganar el torneo
En plata y en bronce creo que es evidente}

        tvaux=array[1..6] of integer;

        trveceseleg=record
                id:integer;
                cantveces:byte;
                end;

        tvveceseleg=array [1..JUGTOTAL] of trveceseleg;


        trjugadorpuntaje=record
                        id:integer;
                        nom:string[30];
                        equipo_real:string[15];
                        posicion:tposicion;
                        ptosfecha:shortint;
                        ptostotal:integer;
                        end;
{Este registro nos va a servir para guardar la informacion del jugador y la informacion  de los puntajes}

        tvjugadorespuntajes=array[1..JUGTOTAL] of trjugadorpuntaje;
//Este arreglo sirve para guardar todos los puntajes por fecha y del torneo para hacer reportes y demas

        tvjugequipo=array[1..MAXJUGEQUI] of trjugadorpuntaje;
//Este arreglo sirve para crear un equipo, con registros del tipo trjugadorpuntaje

        trequiposreales=record
                equiporeal:string[15];
                cant:byte;
                end;

        tvequiposreales=array[1..CANTEQUIREAL] of trequiposreales;

        tvtactica=array[1..5] of string;

        trequipo=record
                nombre:string[20];
                id:byte;
                tact:byte;
                jugadores:tvjugequipo;
                presupuestofecha:real;
                presupuestototal:real;
                cambiosporfecha:byte;
                cambiostotales:byte;
                end;
{Este record guarda toda la informacion necesaria del equipo}
        trusuario=record
                nom:string[20];
                ape:string[15];
                hincha:string[15];
                usuario:string[15];
                pass:string[15];
                admin:boolean;
                equipo:trequipo;
                puntfech:integer;
                punttotal:integer;
                fechaganadas:byte;
                apuestafecha:real;
                apuestatotal:real;
                end;

        tvusuarios=array[1..MAXUSUAR] of trusuario;

        trindex=record
			equi_real:string[15];
			cantjug:byte;
			posprimerjugequipo:integer;
			end;
{El regristro guarda toda la inforamcion del usuario, incluyendo el equipo,
y todos los detalles necesarios para los reportes y el vector sirve para aconglomerar a los usuarios en un lugar}
	tvindex=array[1..CANTEQUIREAL] of trindex;

        TAjugadores=text;


//Esta funcion verifica si hay un usuario con el mismo nombre
//Recibe el vector con el registro de los usuarios y el nombre que desea ingresar
function validarnombreusuario(var usuarios:tvusuarios; nombre:string; numusu:byte;var usuario:byte):boolean;
//Devuelve un booleano verdadero si hay un usuario con el mismo nombre.

//Esta funcion verifica la contrase�a sea correcta
//Recibe la contrase�a y el registro con toda la informacion de los usuarios
function validarpass(contrasenia:string;var usuarios:tvusuarios;usuario:byte):boolean;
//Devuelve un booleano verdadero si es correcta la contrase�a

//Esta funcion ingresea la contrase�a mostrando solo *
//Recibe un string por referencia y lo cambia
procedure ingresarcontrasenia(var contrasenia:string);
//No devuelve nada pero guarda la contrase�a deseada.

//Este procedimiento crea el usuario ingresando cada parte en el registro
//Recibe por referencia el vector de registros usuarios para llenar y ademas un booleano para verificar que el admin no este creado
procedure crearusuario(var usuarios:tvusuarios; var admin:boolean;var numusu:byte;var sesion:boolean; var ususesio:byte);
//No devuelve nada, pero una vez creado el admin debe cambiar el booleano.


{Estas funciones sirve tanto para crear eqeuipo como para realizar cambios
 Cada una va a determinar una condicion de las 3 que tiene que cumplir el equipo}

//Esta verifica que el jugador pueda entrar en el modelo tactico seleccionado
//Recibe el jugador y el vector con el equipo
//hago comentario aparte para recordar que esta es del tipo tjugador y no tjugadorpuntaje
function validarjugposicion (jugador:tjugador;var equipo:trequipo):boolean;
//Devuelve un booleano verdadero si el jugador no rompe la regla

//Esta verifica que el jugador no exceda el presupuesto
//Recibe el jugador y el vector con el registro del equipo donde esta el presupuesto
function validarjugpresupuesto(jugador:tjugador; equipo:trequipo):boolean;
//Devuelve un booleano verdadero si el jugador no rompe la regla

//Esta verifica que el jugador no rompa la regla de jugadores por equipo
//Recibe el jugador y el vector con el registro del equipo
function validarjugequipo(var jugador:tjugador;var equipo:trequipo;var equiposreales:tvequiposreales):boolean;
//Devuelve un booleano verdadero si el jugador no rompe la regla

//Esta funcion sirve para crear el equipo
//Recibe un vector del tipo equipo por referencia
procedure CrearEquipo(var usuario:tvusuarios;var tactica:tvtactica;ususesio:byte;equiposreales:tvequiposreales;var numusu:byte);
//No devuelve nada, pero llena el eqeuipo cumpliendo con las condiciones;

//Este procidimiento es para el administrador solo y configurar las apuestas
//Recibe un registro trapuestas
procedure configurarapuestas(var apuestas:trapuestas);
//No devuelve nada, pero guarda la configuracion de apuestas

//Esta muestra el resultado de las apuestas
//Recibe el vector con la configuracion de las apuestas y la informacion de los usuarios
procedure mostrarapuestas(var usuarios:tvusuarios; var apuestas:trapuestas;var numusu:byte);
//Muestra el resultado de las apuestas en pantalla, no devuelve nada

//Este muestra los mejores jugadores del torneo(global)
//Recibe el vector con los puntajes de todos los jugadores
procedure mejoresjugadores(var jugadores:tvjugadorespuntajes);

//Este muestra los mejores jugadores del torneo(global) por posicion
//Recibe el vector con los puntajes de todos los jugadores
procedure mejoresjugadoresposicion(var jugadores:tvjugadorespuntajes);

//Este muestra el equipo favorito de los usuarios
//Recibe el registro de los usuarios
procedure equipofavorito(var usuario:tvusuarios;numusu:byte; var jugadorespuntajes:tvjugadorespuntajes);
//Muestra el equipo favorito dependiendo de la tactica favorita


//Este muestra la tabla de posicion del torneo
//Recibe el registro de usuarios
procedure tabladeposiciones(var usuarios:tvusuarios);
//Muestra la tabla, con todo lo que pide.

//Este muestra el resultado de la fecha
//Recibe el vector con los puntajes
procedure resultadofecha(var jugadores:tvjugadorespuntajes);
//Muestra los mejores 30 jugadores

//Este sirve para mostrar el menu con los distintos reportes a mostrar
//Recibe el vector con el registro de los usuarios, el vector con los puntajes de los jugadores
procedure mostrarreportes(var usuarios:tvusuarios;var jugadores:tvjugadorespuntajes;var apuestas:trapuestas;numusu:byte);
//no devuelve nada

//Este obtiene el puntaje de los jugadores
//Recibe el vector de jugadorespuntajes por referencia
procedure puntajesjugadores(var jugadores:tvjugadorespuntajes);
//No devuelve nada, pero guarda todos los puntajes

//Este obtiene el puntaje de los equipos
//Recibe el vector con el puntaje de los jugadores y por referencia el vector registro usuarios
procedure puntajesequipos(var usuarios:tvusuarios;var jugadores:tvjugadorespuntajes;numusu:byte);
//No devuelve nada, pero guarda el puntaje de los equipos

//Este realiza los cambios en el equipo
//Recibe Solo el vector de equipo por referencia
procedure realizarcambios(var equipo:trequipo;var tactica:tvtactica;var equiposreales:tvequiposreales);
//No devuelve nada, pero hace todos los cambios pertinentes cumpliendo las condiciones

//Este realiza el inicio de sesion
//Recibe el registro de usuarios
function iniciarsesion(var usuarios:tvusuarios; numusu:byte):byte;
//Lo que devuelve en forma de byte es el id del participante
{En esta funcion, pide el nombre de usuario y la contrase�a,
despues de verificar que es correcto devuelve el id que identifica al usuario}


IMPLEMENTATION

procedure limpiarjugador(var jugador:trjugadorpuntaje);

begin
        jugador.id:=0;
        jugador.nom:='';
        jugador.equipo_real:='';
        jugador.ptosfecha:=0;
        jugador.ptostotal:=0;
end;

procedure limpiarequipo(var equipo:tvjugequipo);

var     i:byte;

begin
        for i:=1 to MAXJUGEQUI do
        begin
                limpiarjugador(equipo[i]);
        end;
end;


procedure cambiojugadores (var jugador1:trjugadorpuntaje;var jugador2:trjugadorpuntaje);
var     auxiliar:trjugadorpuntaje;

begin
        auxiliar:=jugador1;
        jugador1:=jugador2;
        jugador2:=auxiliar;
end;

procedure ordenarid (var jugadores:tvjugadorespuntajes);
var     i,j:integer;

begin
        for i:=1 to JUGTOTAL-1 do
        begin
                for j:=i+1 to JUGTOTAL do
                begin
                        if jugadores[i].id>jugadores[j].id then
                                cambiojugadores(jugadores[i],jugadores[j]);
                end;
        end;
end;

function validarnombreusuario (var usuarios:tvusuarios; nombre:string; numusu:byte; var usuario:byte):boolean;

var     i:byte;
        b:boolean;

begin
        i:=0;
        b:=false;
        while (i<numusu) and (not b) do
        begin
                inc(i);
                if nombre=usuarios[i].usuario then
                        b:=true;
        end;
        usuario:=i;
        validarnombreusuario:=b;
end;


procedure ingresarcontrasenia(var contrasenia:string);

 var
       caracter:char; cont:byte;
 begin
  cont:=0;
  contrasenia:='';

  repeat

   caracter:=readkey;
    if caracter<>#13 then
     begin
      write('*');
      contrasenia:=contrasenia+caracter;
      inc(cont);
     end;

  until (cont=10) or (caracter=#13);

 end;

function validarpass(contrasenia:string;var usuarios:tvusuarios;usuario:byte):boolean;

   var passok:boolean;

       begin
       passok:=false;

       if contrasenia=usuarios[usuario].pass then
               passok:=true
            else
                passok:=false;
       validarpass:=passok;
       end;


function iniciarsesion(var usuarios:tvusuarios;numusu:byte):byte;

var nombre:string[10];
    contrasenia:string[15];
    cont,usuario:byte;
    caracter:char;

    begin
         clrscr;
             repeat
                         writeln ('Ingrese nombre de usuario');
                         readln (nombre);

                        if validarnombreusuario(usuarios,nombre,numusu,usuario) then
                                     begin

                                         cont:=0;
                                         contrasenia:='';
                                       writeln('Ingrese contrasenia');

                                            repeat

                                                caracter:=readkey;
                                               if caracter<>#13 then
                                                  begin
                                                     write('*');
                                                     contrasenia:=contrasenia+caracter;
                                                     inc(cont);
                                                  end;

                                             until (cont=10) or (caracter=#13);

                                     if validarpass(contrasenia,usuarios,usuario) then
                                          iniciarsesion:=usuario
                                     else
                                        writeln('Inicio de sesion incorrecto');
                                     end;

               until validarnombreusuario(usuarios,nombre,numusu,usuario) and validarpass(contrasenia,usuarios,usuario);
end;




procedure crearusuario(var usuarios:tvusuarios; var admin:boolean; var numusu:byte;var sesion:boolean;var ususesio:byte);

var     contrasena:string[15];
        nombre:string[15];
        usuario:byte;

begin
      clrscr;
        inc(numusu);
        writeln('Ingrese su nombre');
        readln(usuarios[numusu].nom);
        writeln('Ingrese su apellido');
        readln(usuarios[numusu].ape);
        writeln('De que cuadro es?');
        readln(usuarios[numusu].hincha);
        writeln('Eliga un nombre de usuario, no mas de 15 caracteres.');
               if numusu<>1 then
                 begin
                    repeat
                        readln(nombre);
                          if validarnombreusuario(usuarios,nombre,numusu,usuario) then
                             begin
                                writeln('Ingrese un nuevo nombre, ya existe un usuario con ese nombre');
                             end;
                        until not validarnombreusuario(usuarios,nombre,numusu,usuario);
                        usuarios[numusu].usuario:=nombre;
                end
        else
           begin
                readln(nombre);
                usuarios[numusu].usuario:=nombre;
           end;
        repeat
                writeln('Ingrese la contrase�a que desea, no mas de 15 caracteres');
                ingresarcontrasenia(usuarios[numusu].pass);
                writeln;
                writeln('Vuelva a ingresar su contrase�a para confirmar');
                ingresarcontrasenia(contrasena);
        until  validarpass (contrasena,usuarios, numusu);
        if admin then
        begin
                usuarios[numusu].admin:=admin;
                admin:=false;
        end;
        sesion:=true;
        ususesio:=numusu;
end;

 function validarmenu (var opc:char):boolean;

 var
 x,cod:byte;
 valido:boolean;

     begin

        val(opc,x,cod);
        begin
        if (cod=0) and (x<=opcvalida) then

                valido:=true
                 else
                valido:=false;

        end;
        validarmenu:=valido;
     end;





  procedure mostrarxposicion(var jugador:tjugador; var puesto:tposicion);

var
   j,n:integer;
    car:char;

begin
        clrscr;
     n:=leer40;
     j:=1;
      repeat

     while j<=n do

             begin

             obtenerjugador(j,jugador);
             if (jugador.posicion=puesto) then
                 begin
                   writeln(jugador.id:3,'  ', jugador.nombre:27,'   ',jugador.precio:8:0,'   ',jugador.equipo_real:10,'   ',jugador.posicion:10);
                 end;
                 inc(j);

             end;
             j:=n;
             n:=n+leer40;

             writeln('presione enter para seguir mostrando jugadores');
             car:=readkey
      until (car<>chr(13)) or (j>=Njugadores{la idea es que salga del ciclo una vez mostrados todos, este numero podria ser otro tranquilamente});

end;




  procedure mostrar22(var id:integer;var jugador:tjugador);

var
car:char;
n,i:integer;


puesto:Tposicion;

begin
 clrscr;

        repeat

                for i:=n to n+tamaniopantalla{un n� mas grande haria que no se lean todos en la pantalla por defecto de pascal} do
                        begin
                        obtenerjugador(i,jugador);
                        writeln(jugador.id:3,'  ', jugador.nombre:27,'   ',jugador.precio:8:0,'   ',jugador.equipo_real:10,'   ',jugador.posicion:10);
                        end;
                n:=n+tamaniopantalla+1{lo mismo, el +1 es para que no muestre el mismo 2 veces};
                writeln('presione enter para seguir mostrando jugadores');
                car:=readkey;

        until (car<>chr(13)) or (n>=610);


end;



 Procedure QuePosicionMostrar (var opc:char);
var
jugador:tjugador;
puesto:tposicion;


 begin
            clrscr;
            writeln('1. Arqueros');
            writeln('2. Defensores');
            writeln('3. Mediocampistas');
            writeln('4. Delanteros');
            opc:=readkey;
         if validarmenu(opc) then

            case opc of
                '1':begin
                     puesto:=ARQUERO;
                     mostrarxposicion(jugador,puesto);
                    end;

                '2':begin
                     puesto:=DEFENSOR;
                     mostrarxposicion(jugador,puesto);
                    end;

                '3':begin
                     puesto:=MEDIOCAMPISTA;
                     mostrarxposicion(jugador,puesto);
                    end;

                '4':begin
                     puesto:=DELANTERO;
                     mostrarxposicion(jugador,puesto);
                    end;
            end

         else
         writeln('ingrese una opcion correcta: 1-4');

end;





 procedure MenuComoMostrar (var opc: char);

var
id:integer;
jugador:tjugador;
puesto:tposicion;

begin
        clrscr;
        writeln('1. Mostrar todos los jugadores');
        writeln('2. Mostrar por posicion');
        opc:=readkey;

         case opc of
        '1': mostrar22(id,jugador);
        '2': QuePosicionMostrar (opc);

         end;

end;


function validarnombre(var nombre:string):boolean;

var
valido:boolean;

begin

   if (length(nombre)<=20) and (length(nombre)>=1) then
        valido:=true;


   validarnombre:=valido;

end;


function validarotrosnombres(var usuario:tvusuarios;var ususesio:byte):boolean;

var
    valido:boolean;
    i:byte;

begin
    valido:=true;
    while (i<=maxusuar) and (valido) do
        begin
        if usuario[ususesio].equipo.nombre=usuario[i].equipo.nombre then
            valido:=false
        else
            inc(i);
        end;
    validarotrosnombres:=valido;
end;

procedure solicitarnombre (var usuario:tvusuarios;var ususesio:byte);

var
    nombre:string[20];


begin
clrscr;
    writeln('Ingrese el nombre del equipo, 20 caracteres maximo');
    repeat
        readln(nombre);
        if (validarnombre(nombre)) and (validarotrosnombres(usuario,ususesio)) then
            usuario[ususesio].equipo.nombre:=nombre
        else
         writeln('opcion incorrecta, ingrese un nombre valido');


    until (validarnombre(nombre));
end;



procedure inicializarTvequiposreales(var equiposreales:tvequiposreales);

var
    i:byte;

begin
    for i:=1 to cantequireal do
      begin
         equiposreales[i].equiporeal:=' ';
         equiposreales[i].cant:=0;
      end;

end;



function Validarjugequipo(var jugador:tjugador;var equipo:trequipo;var equiposreales:tvequiposreales):boolean;

var
    i,j:byte;
    max:boolean;

begin
    inicializartvequiposreales(equiposreales);
    i:=1;
    max:=false;
    while (i<=maxjugequi) and not (max) do
        begin
            readln(equipo.jugadores[i].equipo_real);
            j:=1;
                while (j<=cantequireal) and not (max) do

                   begin

                        if ((equipo.jugadores[i].equipo_real)=(equiposreales[j].equiporeal)) or (equiposreales[j].cant=0) then
                            begin
                                inc (equiposreales[j].cant);
                                inc (i);
                                if equiposreales[j].cant>=3 then
                                    max:=true;
                            end
                         else
                            inc(j);
                   end;
         end;
    if max then
        validarjugequipo:=false
    else
        validarjugequipo:=true;

end;


procedure contarjugposicion(var equipo:trequipo;var arq,def,med,del:byte);

var     i:byte;

begin
        arq:=0;
        def:=0;
        med:=0;
        del:=0;
        for i:=1 to MAXJUGEQUI do
        begin
                if equipo.jugadores[i].id<>0 then
                begin
                        case equipo.jugadores[i].posicion of
                                ARQUERO:inc(arq);
                                DEFENSOR:inc(def);
                                MEDIOCAMPISTA:inc(med);
                                DELANTERO:inc(del);
                        end;
                end;
        end;
end;

function determinartactica(var tactica:tvtactica; var equipo:trequipo):byte;

var     arq,def,med,del,i:byte;

begin
        contarjugposicion(equipo,arq,def,med,del);
        if def=4 then
        begin
                if med=5 then
                        equipo.tact:=1
                else if med=6 then
                        equipo.tact:=2;
        end
        else if def=5 then
        begin
                if med=4 then
                        equipo.tact:=3
                else if med=5 then
                        equipo.tact:=4
                else if med=6 then
                        equipo.tact:=5;
        end;
end;



function validarjugpresupuesto(jugador:tjugador;equipo:trequipo):boolean;



 begin

  equipo.presupuestototal:=equipo.presupuestototal+jugador.precio;

  if (equipo.presupuestototal<=MAXPRES) then

    Validarjugpresupuesto:=true

   else

   Validarjugpresupuesto:=false;

 end;


function validarjugposicion(jugador:tjugador;var equipo:trequipo):boolean;

var     arq,def,med,del:byte;
        b:boolean;

begin
        b:=false;
        contarjugposicion(equipo,arq,def,med,del);
        case jugador.posicion of
                ARQUERO:
                        begin
                               if arq<2 then
                                        b:=true;
                        end;
                DEFENSOR:
                        begin
                                case def of
                                        0,1,2,3:b:=true;
                                        4:
                                        begin
                                                case med of
                                                        0,1,2,3,4:b:=true;
                                                        5:
                                                        begin
                                                                if (del<=3) then
                                                                        b:=true;
                                                        end;
                                                        6:
                                                        begin
                                                                if (del<=2) then
                                                                        b:=true;
                                                        end;
                                                end;
                                        end;
                                end;
                        end;
                MEDIOCAMPISTA:
                        begin
                                case med of
                                        0,1,2,3:b:=true;
                                        4:
                                        begin
                                                case def of
                                                        0,1,2,3,4:b:=true;
                                                        5:
                                                        begin
                                                                if del<4 then
                                                                        b:=true;
                                                        end;
                                                end;
                                        end;
                                        5:
                                        begin
                                                case def of
                                                        0,1,2,3:b:=true;
                                                        4:
                                                        begin
                                                                if del<4 then
                                                                        b:=true;
                                                        end;
                                                        5:
                                                        begin
                                                                if del<3 then
                                                                        b:=true;
                                                        end;
                                                end;
                                        end;
                                end;
                        end;
                DELANTERO:
                        begin
                                case del of
                                        0,1:b:=true;
                                        2:
                                        begin
                                                case def of
                                                        0,1,2,3,4:b:=true;
                                                        5:
                                                        begin
                                                                if med<6 then
                                                                        b:=true;
                                                        end;
                                                end;
                                        end;
                                        3:
                                        begin
                                                case def of
                                                        0,1,2,3:b:=true;
                                                        4:
                                                        begin
                                                                if med<6 then
                                                                        b:=true;
                                                        end;
                                                        5:
                                                        begin
                                                                if med<5 then
                                                                        b:=true;
                                                        end;
                                                end;
                                        end;
                                end;
                        end;
        end;
        validarjugposicion:=b;
end;

procedure AmiRegistro(var jugador:tjugador;var jugadorpuntaje:trjugadorpuntaje);


begin
    jugadorpuntaje.id:=jugador.id;
    jugadorpuntaje.nom:=jugador.nombre;
    jugadorpuntaje.equipo_real:=jugador.equipo_real;
    jugadorpuntaje.posicion:=jugador.posicion;

end;


procedure solicitarjugadores (var jugador:tjugador; var equipo:trequipo;var equiposreales:tvequiposreales);
var
    i:byte;
    id:byte;
    opc:char;
begin
    i:=0;
    while (i<=maxjugequi) do
    begin
        writeln('elija un jugador');
        repeat
                Menucomomostrar(opc);
                writeln('Desea ver jugadores? S/N');
                opc:=readkey;
        until (upcase(opc)='N');
        writeln('Ingrese el id');
        readln(id);

        obtenerjugador(id,jugador);
        if (validarjugposicion(jugador,equipo)) and (validarjugpresupuesto(jugador,equipo))
        and (validarjugequipo(jugador,equipo,equiposreales)) then
           begin
                 Amiregistro(jugador, equipo.jugadores[i]);
                  inc(i);
           end

        else if not (validarjugposicion(jugador,equipo)) then
            writeln ('no sigue ninguna tactica, Recuerde: 3-4-, 3-5-2, 4-3-3, 4-4-2, 4-5-1')

        else if not (validarjugpresupuesto(jugador,equipo)) then
            writeln ('no tiene dinero suficiente')

        else if not (validarjugequipo(jugador,equipo,equiposreales)) then
            writeln ('hay mas de 3 jugadores de un mismo equipo');

    end;
end;





procedure realizarcambios (var equipo:trequipo;var tactica:tvtactica;var equiposreales:tvequiposreales);

{ESTE PROCEDIMIENTO SE VA A PODER REALIZAR 4 VECES CADA 2 FECHAS}

var

i,indice:byte;
id:integer;
jugador:tjugador;
cambios:byte;
opc:char;
begin
    cambios:=0;


    writeln('A que jugador desea cambiar?');
        for i:=1 to maxjugequi do

        begin
            writeln (equipo.jugadores[i].id:3, equipo.jugadores[i].nom:25, equipo.jugadores[i].equipo_real:10, equipo.jugadores[i].posicion:10);
        end;
    readln(indice);
    limpiarjugador(equipo.jugadores[indice]);
    writeln('ha eliminado a ',equipo.jugadores[indice].nom);
    writeln('elija un jugador');
    repeat
        Menucomomostrar(opc);
        writeln('Desea ver jugadores? S/N');
        opc:=readkey;
    until (upcase(opc)='N');
    writeln('Ingrese el id del jugador');
    readln(id);
    obtenerjugador(id,jugador);

   if (validarjugposicion(jugador,equipo)) and (validarjugpresupuesto(jugador,equipo)) and (validarjugequipo(jugador,equipo,equiposreales)) then

    begin

        Amiregistro(jugador,equipo.jugadores[id]);
        inc(equipo.cambiostotales);
        inc(equipo.cambiosporfecha);

    determinartactica(tactica,equipo);
    end;

end;

procedure CrearEquipo(var usuario:tvusuarios;var tactica:tvtactica;ususesio:byte;equiposreales:tvequiposreales;var numusu:byte);

var
jugador:tjugador;
equipo:trequipo;


begin
    limpiarequipo(equipo.jugadores);
    usuario[ususesio].equipo.id:=ususesio;
    solicitarnombre(usuario,ususesio);
    solicitarjugadores(jugador,equipo,equiposreales);
    determinartactica(tactica,equipo);
end;

procedure top10arq(var jugadores:tvjugadorespuntajes);

var i,j,top:integer;

      begin
clrscr;
        top:=0;

          for i:=1 to JUGTOTAL-1 do
             begin
                for j:=i+1 to JUGTOTAL do
                   begin
                        if jugadores[i].ptostotal<jugadores[j].ptostotal then
                                cambiojugadores(jugadores[i],jugadores[j]);
                   end;
             end;

        writeln('Jugador --- Club --- Acumulado');
        i:=0;
        repeat
             begin
             inc(i);
               if jugadores[i].posicion=ARQUERO then
                  writeln(jugadores[i].nom,' --- ',jugadores[i].equipo_real,' --- ',jugadores[i].ptostotal);
                  inc(top);
             end;

        until top=10;

      end;


procedure top10def(var jugadores:tvjugadorespuntajes);

var i,j,top:integer;

      begin
clrscr;
        top:=0;

          for i:=1 to JUGTOTAL-1 do
             begin
                for j:=i+1 to JUGTOTAL do
                   begin
                        if jugadores[i].ptostotal<jugadores[j].ptostotal then
                                cambiojugadores(jugadores[i],jugadores[j]);
                   end;
             end;

        writeln('Jugador --- Club --- Acumulado');
        i:=0;
        repeat
             begin
             inc(i);
               if jugadores[i].posicion=DEFENSOR then
                  writeln(jugadores[i].nom,' --- ',jugadores[i].equipo_real,' --- ',jugadores[i].ptostotal);
                  inc(top);
             end;

        until top=10;

      end;

procedure top10med(var jugadores:tvjugadorespuntajes);

var i,j,top:integer;

      begin
clrscr;
        top:=0;

          for i:=1 to JUGTOTAL-1 do
             begin
                for j:=i+1 to JUGTOTAL do
                   begin
                        if jugadores[i].ptostotal<jugadores[j].ptostotal then
                                cambiojugadores(jugadores[i],jugadores[j]);
                   end;
             end;

        writeln('Jugador --- Club --- Acumulado');
        i:=0;
        repeat
             begin
             inc(i);
               if jugadores[i].posicion=MEDIOCAMPISTA then
                  writeln(jugadores[i].nom,' --- ',jugadores[i].equipo_real,' --- ',jugadores[i].ptostotal);
                  inc(top);
             end;

        until top=10;

      end;


procedure top10del(var jugadores:tvjugadorespuntajes);

var i,j,top:integer;

      begin
clrscr;
        top:=0;

          for i:=1 to JUGTOTAL-1 do
             begin
                for j:=i+1 to JUGTOTAL do
                   begin
                        if jugadores[i].ptostotal<jugadores[j].ptostotal then
                                cambiojugadores(jugadores[i],jugadores[j]);
                   end;
             end;

        writeln('Jugador --- Club --- Acumulado');
        i:=0;
        repeat
             begin
             inc(i);
               if jugadores[i].posicion=DELANTERO then
                  writeln(jugadores[i].nom,' --- ',jugadores[i].equipo_real,' --- ',jugadores[i].ptostotal);
                  inc(top);
             end;

        until top=10;

      end;

procedure mejoresjugadoresposicion(var jugadores:tvjugadorespuntajes);

var op:char; salir:boolean;



    begin
clrscr;
    salir:=false;
      repeat
         writeln('Ingrese el resporte que desea ver:');
         writeln('Tops 10');
         writeln('1.Arqueros');
         writeln('2.Defensores');
         writeln('3.Mediocampistas');
         writeln('4.Delanteros');
         writeln;
         writeln('5.Volver a reportes');
         op:=readkey;

           if op<>'5' then
              begin
                 clrscr;
                 case op of
                    '1': top10arq(jugadores);
                    '2': top10def(jugadores);
                    '3': top10med(jugadores);
                    '4': top10del(jugadores);
                 else
                    writeln('Incorrecto');
                 end;
              end;
         if op='5'then
          salir:=true;

       until salir;
    end;

procedure tactfavorita(var usuario:tvusuarios;var numusu:byte;var maxdef:byte;var maxmed:byte;var maxdel:byte);

var

tvtact:array[1..5] of byte;
 i,max:byte;


         begin

               max:=0;

               for i:=1 to 5 do

                        tvtact[i]:=0;

               for i:=1 to numusu do

                       inc(tvtact[usuario[i].equipo.tact]);

               for i:=1 to 5 do

                       if tvtact[i]>max then
                                max:=i;


              case max of
                1: begin
                        maxdef:=4;
                        maxmed:=5;
                        maxdel:=4;
                   end;
                2: begin
                        maxdef:=4;
                        maxmed:=6;
                        maxdel:=3;         {Maximo jugadores por posicion incluido suplentes}
                   end;
                3: begin
                        maxdef:=5;
                        maxmed:=4;
                        maxdel:=4;
                   end;
                4: begin
                        maxdef:=5;
                        maxmed:=5;
                        maxdel:=3;
                   end;
                5: begin
                        maxdef:=5;
                        maxmed:=6;
                        maxdel:=2;
                   end;
                end;


         end;


procedure iniciar_veceseleg(var veceseleg:tvveceseleg);

var
        i:integer;
begin
        for i:=1 to JUGTOTAL do      {Asigna una id al vector veceselegido para poder ordenarlos e inicializa .ptos}
                begin
                        veceseleg[i].id:=i;
                        veceseleg[i].cantveces:=0;
                end;
end;

procedure veceselegidojug(var usuario:tvusuarios;var veceseleg:tvveceseleg;var numusu:byte);

var
        i,j:byte;

begin
        for i:=1 to numusu do
                for j:=1 to MAXJUGEQUI do

                       inc(veceseleg[usuario[i].equipo.jugadores[j].id].cantveces);


end;

procedure ordenarxpos_veceseleg(var veceseleg:tvveceseleg;var numusu:byte);

var
      i,j:byte;  aux:trveceseleg;

begin
        for i:=1 to (numusu-1) do
                for j:=(i+1) to numusu do
                        if (veceseleg[i].cantveces<veceseleg[j].cantveces) then
                                begin
                                        aux:=veceseleg[i];
                                        veceseleg[i]:=veceseleg[j];
                                        veceseleg[j]:=aux;
                                end;
end;


procedure mostrar_eq(var veceseleg:tvveceseleg;var jugadorespuntaje:tvjugadorespuntajes;var maxdef:byte;var maxmed:byte;var maxdel:byte);

type
        tvfavorito=array[1..MAXJUGEQUI] of trveceseleg;
var
        vfavorito:tvfavorito;
        contarq,contdef,contmed,contdel,maxarq,pos_vfavorito:byte;
        I:integer;
        auxarq,auxdef,auxmed,auxdel:trveceseleg;
begin
      maxarq:=2;
      contarq:=0;
      contdef:=0;
      contmed:=0;
      contdel:=0;
      pos_vfavorito:=1;

      i:=1;

      while (i<=JUGTOTAL) or (contarq=maxarq) do
                begin
                        if jugadorespuntaje[veceseleg[i].id].posicion=ARQUERO  then
                                begin
                                        if contarq<maxarq then
                                             begin
                                                   vfavorito[pos_vfavorito]:=veceseleg[i];
                                                   inc(pos_vfavorito);
                                             end

                                        else if contarq=maxarq then
                                                 auxarq:=veceseleg[i];
                                        inc(contarq);
                                end;
                       inc(i);
                end;

      i:=1;

      while (i<=JUGTOTAL) or (contdef=maxdef) do
        begin
                if jugadorespuntaje[veceseleg[i].id].posicion=DEFENSOR then
                        begin
                                if contdef<maxdef then
                                        begin
                                                vfavorito[pos_vfavorito]:=veceseleg[i];
                                                inc(pos_vfavorito);
                                        end
                                else if contdef=maxdef then
                                        auxdef:=veceseleg[i];
                                inc(contdef);
                        end;
               inc(i);
        end;

      i:=1;

      while (i<=JUGTOTAL) or (contmed=maxmed) do
        begin
                if jugadorespuntaje[veceseleg[i].id].posicion=MEDIOCAMPISTA then
                        begin
                                if contmed<maxmed then
                                        begin
                                                vfavorito[pos_vfavorito]:=veceseleg[i];
                                                inc(pos_vfavorito);
                                        end
                                else if contmed=maxmed then
                                        auxmed:=veceseleg[i];
                                inc(contmed);
                        end;
                inc(i);
        end;

      i:=1;

      while (i<=JUGTOTAL) or (contdel=maxdel) do
        begin
                if jugadorespuntaje[veceseleg[i].id].posicion=DELANTERO then
                        begin
                                if contdel<maxdel then
                                        begin
                                                vfavorito[pos_vfavorito]:=veceseleg[i];
                                                inc(pos_vfavorito);
                                        end
                                else if contdel=maxdel then
                                        auxdel:=veceseleg[i];
                                inc(contdel);
                        end;
                inc(i);
        end;

      vfavorito[12]:=auxarq;
      vfavorito[13]:=auxdef;
      vfavorito[14]:=auxmed;
      vfavorito[15]:=auxdel;


      writeln('Jugador                   Posici�n       Veces Elegido ');
      for pos_vfavorito:=1 to MAXJUGEQUI do
        writeln(jugadorespuntaje[vfavorito[i].id].nom:26,jugadorespuntaje[vfavorito[i].id].posicion:15,vfavorito[i].cantveces:3);

end;

procedure equipofavorito(var usuario:tvusuarios;numusu:byte;var jugadorespuntajes:tvjugadorespuntajes);


var
   veceseleg:tvveceseleg; maxdef,maxmed,maxdel:byte;

begin

        tactfavorita(usuario,numusu,maxdef,maxmed,maxdel);
        iniciar_veceseleg(veceseleg);
        veceselegidojug(usuario,veceseleg,numusu);
        ordenarxpos_veceseleg(veceseleg,numusu);
        mostrar_eq(veceseleg,jugadorespuntajes,maxdef,maxmed,maxdel);
        readkey;
end;

procedure Ordenarxpuntaje(var usuarios:tvusuarios);
var
i,j:byte;
aux:trusuario;

begin

    for i:=1 to maxusuar-1 do
     for j:=1 to maxusuar-i do
      if (usuarios[j].punttotal < usuarios[j+1].punttotal) then
       aux:=usuarios[j];
       usuarios[j]:=usuarios[j+1];
       usuarios[j+1]:=aux;
end;

procedure ordenarxid (var usuarios:tvusuarios);
var

    i,j:byte;
    aux:trusuario;

begin

    for i:=1 to maxusuar-1 do
     for j:=1 to maxusuar-i do
      if (usuarios[j].equipo.id < usuarios[j+1].equipo.id) then
       aux:=usuarios[j];
       usuarios[j]:=usuarios[j+1];
       usuarios[j+1]:=aux;

end;

procedure presupuestopromedio(var usuarios:tvusuarios;var i:byte);
var
    j:byte;
    acum:real;
    presprom:real;
begin
    acum:=0;
    presprom:=0;

    for j:=1 to fechas do
        begin
            acum:=(usuarios[i].equipo.presupuestofecha + acum);
        end;
    presprom:=(acum / fechas);

         {se lo asigno a presupuestototal xq no lo usamos para nada .. seria mejor cambiarle el nombre a
            presupuestopromedio}
    usuarios[i].equipo.presupuestototal:=presprom;
end;





procedure tabladeposiciones(var usuarios:tvusuarios);
var
i:byte;


begin
  Ordenarxpuntaje(usuarios);
  clrscr;

  for i:=1 to 5 do
   begin
    writeln (usuarios[i].nom,'  ', usuarios[i].equipo.nombre,  ' tiene ',usuarios[i].punttotal, ' gano ',
    usuarios[i].fechaganadas,' fechas, hizo ',usuarios[i].equipo.cambiostotales,' cambios y su presupuesto promedio fue ',
    usuarios[i].equipo.presupuestototal);
   end;
  ordenarxid (usuarios);
  readln;
  writeln('Presione enter para continuar');
end;


function calcularapuestas(var apuestas:trapuestas;var numusu:byte):real;
var
    monto:real;

begin
    monto:=((fechas*apuestas.porfecha)+apuestas.oro+apuestas.plata+apuestas.bronce)/numusu;
    calcularapuestas:=monto;
end;



procedure mostrarapuestas(var usuarios:tvusuarios; var apuestas:trapuestas;var numusu:byte);

var
    i:byte;

begin
    clrscr;
    writeln('apuestas por fecha: ',apuestas.porfecha);
    writeln('medalla de oro: ',apuestas.oro);
    writeln('medalla de plata: ',apuestas.plata);
    writeln('medalla de bronce: ',apuestas.bronce);
    writeln;
    writeln;
    writeln;
    writeln;
    writeln;
    writeln('Apuesta: ',calcularapuestas(apuestas,numusu));




    for i:=1 to numusu do
        writeln('el equipo: ',usuarios[i].equipo.nombre,' del participante ',usuarios[i].nom,' ha ganado $',
                 usuarios[i].fechaganadas*apuestas.porfecha{O YA ESTA CARGADO EN USUARIOS.APUESTAFECHA??}
                 ,' por fechas y por torneo: $',usuarios[i].apuestatotal,' sumando asi un total de: $',
                 usuarios[i].fechaganadas*apuestas.porfecha+usuarios[i].apuestatotal);
          readln;
end;

//Este muestra los mejores jugadores del torneo(global)
//Recibe el vector con los puntajes de todos los jugadores
procedure mejoresjugadores(var jugadores:tvjugadorespuntajes);
//Muestra los mejores jugadores del torneo
var     auxiliar:trjugadorpuntaje;
        i,j:integer;
        c:char;

begin
        for i:=1 to JUGTOTAL-1 do
        begin
                for j:=i+1 to JUGTOTAL do
                begin
                        if jugadores[i].ptostotal<jugadores[j].ptostotal then
                                cambiojugadores(jugadores[i],jugadores[j]);
                end;
        end;
        i:=1;
        c:='s';
        writeln('Jugador --- Puesto --- Club --- Acumulado');
        while (i<=25) and ((c='s') or (c='S')) do
        begin
                writeln('',jugadores[i].nom,' --- ',jugadores[i].posicion,' --- ',jugadores[i].equipo_real,'--- ',jugadores[i].ptostotal);
                if (i mod 5=0) then
                begin
                        writeln('Desea continuar con la lista? Presione s para continuar');
                        readln(c);
                        if (c='s') or (c='S') then
                                writeln('Jugador --- Puesto --- Club --- Acumulado');
                end;
                inc(i);
        end;
        readln;
        ordenarid(jugadores);
end;


procedure inictvindex(var index:tvindex);

var
i:byte;

begin
	for i:=1 to CANTEQUIREAL do
		begin
			index[i].equi_real:=' ';
			index[i].cantjug:=0;
			index[i].posprimerjugequipo:=0;
		end;
end;


function obtenerID(var Ajugadores:TAjugadores):integer;   {TAjugadores es el archivo jugadores.csv}

var
linea:string;
id:integer;
cod:byte;

begin
	reset(Ajugadores);
	while not (EOF(Ajugadores)) do
		begin
			readln(linea);
			id:=Val(copy(linea,1,3),id,cod);
		end;
	obtenerID:=id;
	Close(Ajugadores);

end;


procedure indexar(var Ajugadores:TAjugador; Var index:tvindex; var nindex:integer);

var
id:integer;
jugador:tjugador;

begin
	inictvindex(index);
	reset(Ajugadores);
	nindex:=1;
	while not (EOF(Ajugadores)) do
		begin	
			id:=obtenerid(Ajugadores);
			obtenerjugador(id,jugadore);
				if (index[nindex].equi_real=' ') then
					begin
						inc(index[nindex].cantjug);
						index[nindex].posprimerjugequipo:=jugador.id;
					end;
				if (index[nindex].equi_real:=jugador.equipo_real) then
					inc(index[nindex].cantjug)
				else
					inc(nindex);
		end;
	close(Ajugadores);
end;


Procedure MostrarEquipoReal(Var opc:byte; Var index:tvindex);

var
i:integer;
jugador:tjugador;

begin
	for i:=index[opc].posprimerjugequipo) to (index[opc].posprimerjugequipo + index[opc].cantjug) do
		begin
			obtenerjugador(i,jugador);
            writeln(jugador.id:3,'  ', jugador.nombre:27,'   ',jugador.precio:8:0,'   ',jugador.posicion:10);
			
        end;
end;


 function validaropcionequipo (var opc:string):boolean;

 var
 x,cod:byte;
 valido:boolean;

     begin

        val(opc,x,cod);
        begin
        if (cod=0) and (x<=CANTEQUIREAL) then

                valido:=true
                 else
                valido:=false;

        end;
        validarmenu:=valido;
     end;

	
	
procedure QueEquipoMostrar(var index:tvindex;var Ajugadores:TAjugadores);

var
opc:string;

begin
   clrscr;
   Repeat
	writeln('Que plantel desea ver?');
	writeln('1.All Boys');
	writeln('2.Boca');
	writeln('3.Banfield');
	writeln('4.Argentinos');
	writeln('5.Arsenal');
	writeln('6.Colon');
	writeln('7.Estudiantes');
	writeln('8.Gimnasia');
	writeln('9.Godoy Cruz');
	writeln('10.Huracan');
	writeln('11.Independiente');
	writeln('12.Newells');
	writeln('13.Lanus');
	writeln('14.Olimpo');
	writeln('15.Quilmes');
	writeln('16.Racing');
	writeln('17.River Plate');
	writeln('18.San Lorenzo');
	writeln('19.Tigre');
	writeln('20.Velez');
	Readln(opc);
	clrscr;
	until (validaropcionequipo(opc));
	MostrarEquipoReal(opc,index);
end;


procedure mostrarreportes(var usuarios:tvusuarios;var jugadores:tvjugadorespuntajes;var apuestas:trapuestas;numusu:byte);

var

opc:char;

begin

 repeat
 clrscr;
 writeln('                          ***   REPORTES  ***                ');

 writeln;
 writeln('1.  Tabla de Posiciones');
 writeln;
 writeln('2.  Resultado de la fecha');
 writeln;
 writeln('3.  Resultado de las Apuestas');
 writeln;
 writeln('4.  Mejores Jugadores');
 writeln;
 writeln('5.  Mejores Jugadores por Puesto');
 writeln;
 writeln('6.  Equipo Favorito');
 writeln;
 writeln('7.  Jugadores por Club');
 writeln;
 writeln('8.  Menu Anterior');


 opc:=readkey;

 case opc of

   '1': tabladeposiciones(usuarios);
   '2': resultadofecha(jugadores);
   '3': mostrarapuestas(usuarios,apuestas,numusu);
   '4': mejoresjugadores(jugadores);
   '5': mejoresjugadoresposicion(jugadores);
   '6': equipofavorito(usuarios,numusu,jugadores);
   '7': queequipomostrar(index,Ajugadores)
  end;
until opc='8';

end;

procedure configurarapuestas(var apuestas:trapuestas);

	 begin
                 clrscr;
                   writeln('Luego de ingresar cada premio presione enter');
	         repeat

	           write('Ingrese Premio por fecha: ');
		       {$I-}
	           readln(apuestas.porfecha);
                       {$I+}

                 until ioresult=0;

                 repeat

	           write('Ingrese Premio para el campeon: ');
                       {$I-}
	           readln(apuestas.oro);
                       {$I+}

                 until ioresult=0;

                 repeat

	           write('Ingrese Premio para el sub-campeon: ');
                       {$I-}
	           readln(apuestas.plata);
                       {$I+}

                 until ioresult=0;

                 repeat

	           write('Ingrese Premio para Tercer puesto: ');
                       {$I-}
	           readln(apuestas.bronce);
		       {$I+}

	         until ioresult=0;
         end;

procedure puntajesjugadores(var jugadores:tvjugadorespuntajes);

var i:integer;

     begin
         for i:=1 to JUGTOTAL do
           jugadores[i].ptosfecha:=obtenerpuntajejugador(i);
     end;


//Este muestra el resultado de la fecha
//Recibe el vector con los puntajes
procedure resultadofecha (var jugadores:tvjugadorespuntajes);
//Muestra los mejores 30 jugadores
var     i,j:integer;
        c:char;

begin
        for i:=1 to JUGTOTAL-1 do
        begin
                for j:=i+1 to JUGTOTAL do
                begin
                        if jugadores[i].ptosfecha<jugadores[j].ptosfecha then
                                cambiojugadores(jugadores[i],jugadores[j]);
                end;
        end;
        c:='N';
        i:=1;
        writeln('Jugador --- Puesto --- Club --- Puntaje');
        while (i<=30) and ((c='n') or (c='N')) do
        begin
                writeln('',jugadores[i].nom,' --- ',jugadores[i].posicion,' --- ',jugadores[i].equipo_real,'--- ',jugadores[i].ptostotal);
                if (i mod 5=0) then
                begin
                        writeln('Desea volver al menu anterior? Presione n para continuar viendo la lista');
                        readln(c);
                        if (c='n') or (c='N') then
                                writeln('Jugador --- Puesto --- Club --- Acumulado');
                end;
                inc(i);
        end;
        ordenarid(jugadores);
end;

procedure iniciarvaux(var Vaux:tvaux);

var
        indicevaux:byte;
begin
        for Indicevaux:=1 to 6 do
                Vaux[indicevaux]:=0;
end;

procedure ordenarxpos(var usuarios:tvusuarios;var vaux:tvaux;var pos:tposicion;var i:byte);

var
      j,f,g,posvaux,aux:byte;  {f y g los indices para ordenamiento, j para recorrer el vectoe de jugadores del equipo}


        begin
                Posvaux:=1;     {Guardo en vector vaux solo los jugadores de una determinada posicion}

                INICIARVAUX(vaux); {Inicializo el vector}

                for j:=1 to MAXJUGEQUI do
                        if (usuarios[i].equipo.jugadores[j].posicion=pos) then
                                begin
                                       Vaux[Posvaux]:=usuarios[i].equipo.jugadores[j].ptosfecha;
                                       inc(Posvaux);
                                end;
         {Aca ordeno el vector  vaux de ptos.}
               for f:=1 to (6-1) do
                        for g:=(f+1) to 6 do
                                if Vaux[f]<Vaux[g] then
                                     begin
                                        Aux:=Vaux[f];
                                        Vaux[f]:=Vaux[g];
                                        Vaux[g]:=Aux;
                                     end;
        end;

procedure vertactica(var usuarios:tvusuarios;var i:byte;var maxdef:byte;var maxmed:byte;var maxdel:byte);

var
        opc:byte;

begin
        Opc:=usuarios[i].equipo.tact;

                                        {Veo la tactica que tiene el equipo para definir cuantos elementos
                                         tomo del vector para cada tposicion, para sumar los puntajes}
        case Opc of
                1: begin
                        maxdef:=3;
                        maxmed:=4;
                        maxdel:=3;
                   end;
                2: begin
                        maxdef:=3;
                        maxmed:=5;
                        maxdel:=2;
                   end;
                3: begin
                        maxdef:=4;
                        maxmed:=3;
                        maxdel:=3;
                   end;
                4: begin
                        maxdef:=4;
                        maxmed:=4;
                        maxdel:=2;
                   end;
                5: begin
                        maxdef:=4;
                        maxmed:=5;
                        maxdel:=1;
                   end;
        end;
end;


procedure puntajesequipos(var usuarios:tvusuarios;var jugadores:tvjugadorespuntajes;numusu:byte);

var       vaux:tvaux; pos:tposicion;   acum:integer; i,j,maxdef,maxmed,maxdel,def,med,del:byte;

begin
        for j:=1 to Numusu do
          begin

               Acum:=0;

               i:=j;
               VERTACTICA(usuarios,i,maxdef,maxmed,maxdel);

               Pos:=arquero;

               ORDENARXPOS(usuarios,vaux,pos,i);

               Acum:=acum+vaux[1]; {vaux[1] porque solo cuento el primer arquero del vector ordenado}

               Pos:=defensor;

               ORDENARXPOS(usuarios,vaux,pos,i);
               for Def:=1 to maxdef do
                        acum:=acum+vaux[def];

               Pos:=mediocampista;

               ORDENARXPOS(usuarios,vaux,pos,i);
               for Med:=1 to maxmed do
                        acum:=acum+vaux[med];

               Pos:=delantero;

               ORDENARXPOS(usuarios,vaux,pos,i);
               for Del:=1 to maxdel do
                        acum:=acum+vaux[del];

               usuarios[i].puntfech:=acum;    {Guardo el puntaje de la fecha}

               usuarios[i].punttotal:=usuarios[i].punttotal+usuarios[i].puntfech;    {Al puntaje total le sumo el de la fecha}

          end;
end;





begin

end.
