program tpgrupalprogppal;

uses    crt,unidadtp,Aypdt;

function Menu1 (var salida:boolean; admin:boolean):byte;

var     b,cod:byte;
        a:char;

begin
        writeln('Seleccione la opcion que desea');
        if admin then
                writeln('1: Crear Usuario Adinistrador del Torneo')
        else
        begin
                clrscr;
                writeln('1: Crear Usuario');
                writeln('2: Iniciar Sesion');
        end;
        writeln('3: Salir');
        a:=readkey;
        if admin then
        begin
                while (a<>'1')  and (a<>'3') do
                begin
                        writeln('Opcion incorrecta. Ingrese Nuevamente.');
                        a:=readkey;
                end;
        end
        else
        begin
                while (a<>'1') and (a<>'3') and (a<>'2') do
                begin
                         writeln('Opcion Incorrecta. Ingrese nuevamente.');
                         a:=readkey;
                end;
        end;
        val(a,b,cod);
        if cod=0 then
                menu1:=b;
end;

function Menu3 (admin:boolean; torneo:boolean;var sesion:boolean):byte;

var     a:char;
        b,cod:byte;

begin
        clrscr;
        writeln('Seleccione la opcion que desea');
                writeln;
                if not torneo then
                        writeln('1: Crear Equipo');
                writeln('2: Realizar Cambios');
                writeln('3: Reportes');

        if admin then
        begin
                if not torneo then
                   begin
                        writeln('4: Configurar Apuestas');
                        writeln('5: Iniciar Torneo');
                   end;
                        writeln('6: Cargar Calificaciones')
        end;
        writeln('7: Cerrar Sesi�n');
        a:=readkey;
        if admin then
        begin
                if not torneo then
                begin
                        while (a<>'1') and (a<>'2') and (a<>'3') and (a<>'4') and (a<>'5') and (a<>'6') and (a<>'7')  do
                        begin
                                writeln('Opcion Incorrecta. Ingrese Nuevamente.');
                                a:=readkey;
                        end;
                end
                else
                begin
                        while (a<>'2') and (a<>'3') and (a<>'5') and (a<>'6') and (a<>'7') do
                        begin
                                writeln('Opcion Incorrecta. Ingrese Nuevamente.');
                                a:=readkey;
                        end;
                end;
        end
        else
        begin
                if not torneo then
                begin
                        while (a<>'1') and (a<>'2') and (a<>'3') and (a<>'7') do
                        begin
                                writeln('Opcion Incorrecta. Ingrese Nuevamente.');
                                a:=readkey;
                        end;
                end
                else
                begin
                        while (a<>'2') and (a<>'3') and (a<>'7') do
                        begin
                                writeln('Opcion Incorrecta. Ingrese Nuevamente.');
                                a:=readkey;
                        end;
                end;
        end;
        if (a='7') then
                sesion:=false;
        val(a,b,cod);
        if cod=0 then
                menu3:=b;
end;

procedure cambiarfecha(var usuarios:tvusuarios; var jugadores:tvjugadorespuntajes; var fechas:byte; numusu:byte);
var     i,j:byte;

begin
        inc(fechas);
        if (fechas mod 2=0) then
        begin
                for i:=1 to numusu do
                begin
                        usuarios[j].equipo.cambiosporfecha:=0;
                end;
        end;
end;



procedure creartactica(var tactica:tvtactica);
begin
	tactica[1]:='3-4-3';
	tactica[2]:='3-5-2';
	tactica[3]:='4-3-3';
	tactica[4]:='4-4-2';
	tactica[5]:='4-5-1';
end;


var

salida:boolean; // [Determina si el usuario quiere salir, es una manera rapida de terminar el programa]
torneo:boolean; // [Determina si el torneo empezo o no]
admin:boolean; // [Determina si el admin fue creado, una vez que se creo cambia a falso]
fechas:byte; // [Cuenta la cantidad de fechas, se inicializa en 1 cuando empieza en torneo]
numusu:byte; // [Cuenta la cantidad de usuarios que hay. se inicializa en 0 y solo se incremenete cuando el procemiento crear usuario es llamando]
sesion:boolean; // [Determina si hay una sesion iniciada o no]
ususesio:byte; // [Guarda el numero que identifica al usuario que esta iniciado]
opcion:byte; // [Guarda la opcion elegida para redireccionar a los demas, es como un un indicador de a donde vamos]
tactica:tvtactica;
usuarios:tvusuarios;
jugadores:tvjugadorespuntajes;
jugadorpuntaje:trjugadorpuntaje;
jugador:tjugador;
apuestas:trapuestas;
equiposreales:tvequiposreales;


begin
        salida:=false;
        torneo:=false;
        admin:=true;
        fechas:=0;
        numusu:=0;
        creartactica(tactica);
	assign(Ajugadores,'Jugadores.csv');
        while (not salida) and (fechas<20) do
        begin
                opcion:=menu1(salida,admin);
                if not sesion then
                begin
                        case opcion of
                                1:crearusuario(usuarios,admin,numusu,sesion,ususesio);
                                2:
                                begin
                                        ususesio:=iniciarsesion(usuarios,numusu);
                                        sesion:=true;
                                end;
                                3:salida:=true
                        end;
                end;
                while sesion do
                begin
                        opcion:=menu3(usuarios[ususesio].admin,torneo,sesion);
                        case opcion of
                                1:CrearEquipo(usuarios,tactica,ususesio,equiposreales,numusu);
                                2:
                                begin
                                         if torneo then
                                                realizarcambios(usuarios[ususesio].equipo,tactica,equiposreales)
                                        else
                                                clrscr;
                                                writeln('El torneo no empezo por lo que no se pueden realizar cambios todavia.');
                                                writeln('Presione enter para volver al menu anterior');
                                                readln;
                                end;
                                3:begin
                                    if not torneo then
                                       begin
                                        clrscr;
                                        writeln('Torneo no empezo, no hay reportes');
                                        writeln('Presione enter para volver al menu anterior');
                                        readln;
                                       end
                                    else
                                       mostrarreportes(usuarios, jugadores, apuestas, numusu);
                                  end;
                                4: configurarapuestas(apuestas);
                                5:
                                begin
                                        torneo:=true;
                                        clrscr;
                                        writeln('El Torneo ha comenzado');
                                        readln;
                                end;
                                6:
                                begin
                                      if torneo then
                                        begin
                                         puntajesjugadores(jugadores);
                                         puntajesequipos(usuarios, jugadores, numusu);
                                         cambiarfecha(usuarios,jugadores,fechas,numusu);

                                        end
                                      else
                                        clrscr;
                                        writeln('No se pueden cargar calificaciones hasta que no comience el torneo');
                                        writeln('Presione enter para vovler al menu anterior');
                                        readln;

                                end;
                        end;
                end;
        end;
end.


