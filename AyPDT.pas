Unit AyPDT;

  Interface
    Type

    tPosicion = (ARQUERO, DEFENSOR, MEDIOCAMPISTA, DELANTERO);

    tJugador = record
                id : integer;
                nombre : string[30];
                precio : real;
                equipo_real : string[15];
                posicion : tPosicion;
            end;

    tListaJugadores = array [1..623] of tJugador;

    procedure obtenerJugador(id: integer; var jugador: tJugador);

    function jugoPartido(id:integer): boolean;

    function obtenerPuntajeJugador(id: integer): shortint;

  Implementation

    procedure obtenerJugador(id: integer; var jugador: tJugador);
    var
        aux : tJugador;
        jugadores : tListaJugadores;
    begin
        aux.id := 1;
        aux.nombre := 'Cambiasso, Nicolas';
        aux.precio := 3900000;
        aux.equipo_real := 'All Boys';
        aux.posicion := ARQUERO;
        jugadores[1] := aux;

        aux.id := 2;
        aux.nombre := 'Di Grazia, Lucas';
        aux.precio := 400000;
        aux.equipo_real := 'All Boys';
        aux.posicion := ARQUERO;
        jugadores[2] := aux;

        aux.id := 3;
        aux.nombre := 'Giordano, Matias';
        aux.precio := 800000;
        aux.equipo_real := 'All Boys';
        aux.posicion := ARQUERO;
        jugadores[3] := aux;

        aux.id := 4;
        aux.nombre := 'Batista, Matias';
        aux.precio := 300000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DEFENSOR;
        jugadores[4] := aux;

        aux.id := 5;
        aux.nombre := 'Brau, Mariano';
        aux.precio := 1300000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DEFENSOR;
        jugadores[5] := aux;

        aux.id := 6;
        aux.nombre := 'Casteglione, Carlos';
        aux.precio := 4000000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DEFENSOR;
        jugadores[6] := aux;

        aux.id := 7;
        aux.nombre := 'Dominguez, Eduardo';
        aux.precio := 4500000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DEFENSOR;
        jugadores[7] := aux;

        aux.id := 8;
        aux.nombre := 'Madeo, Carlos';
        aux.precio := 1400000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DEFENSOR;
        jugadores[8] := aux;

        aux.id := 9;
        aux.nombre := 'Panceri, Armando';
        aux.precio := 1600000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DEFENSOR;
        jugadores[9] := aux;

        aux.id := 10;
        aux.nombre := 'Soto, Carlos';
        aux.precio := 2300000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DEFENSOR;
        jugadores[10] := aux;

        aux.id := 11;
        aux.nombre := 'Vella, Cristian';
        aux.precio := 2400000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DEFENSOR;
        jugadores[11] := aux;

        aux.id := 12;
        aux.nombre := 'Barrientos, Hugo';
        aux.precio := 3500000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[12] := aux;

        aux.id := 13;
        aux.nombre := 'Grazzini, Sebastian';
        aux.precio := 4300000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[13] := aux;

        aux.id := 14;
        aux.nombre := 'Lopez, Victor David';
        aux.precio := 500000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[14] := aux;

        aux.id := 15;
        aux.nombre := 'Perea, Emanuel';
        aux.precio := 2000000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[15] := aux;

        aux.id := 16;
        aux.nombre := 'Prol, Mauricio';
        aux.precio := 800000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[16] := aux;

        aux.id := 17;
        aux.nombre := 'Rimoldi, Lucas';
        aux.precio := 3600000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[17] := aux;

        aux.id := 18;
        aux.nombre := 'Rodriguez, Juan Pablo';
        aux.precio := 3800000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[18] := aux;

        aux.id := 19;
        aux.nombre := 'Rudler, Matias Ezequiel';
        aux.precio := 400000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[19] := aux;

        aux.id := 20;
        aux.nombre := 'Sanchez, Fernando';
        aux.precio := 2800000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[20] := aux;

        aux.id := 21;
        aux.nombre := 'Tessoro, Facundo';
        aux.precio := 700000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[21] := aux;

        aux.id := 22;
        aux.nombre := 'Vieytes, Marcelo';
        aux.precio := 1800000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[22] := aux;

        aux.id := 23;
        aux.nombre := 'Zarate, Ariel';
        aux.precio := 2800000;
        aux.equipo_real := 'All Boys';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[23] := aux;

        aux.id := 24;
        aux.nombre := 'Bartelt, Gustavo';
        aux.precio := 3800000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DELANTERO;
        jugadores[24] := aux;

        aux.id := 25;
        aux.nombre := 'Ereros, Sebastian';
        aux.precio := 3800000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DELANTERO;
        jugadores[25] := aux;

        aux.id := 26;
        aux.nombre := 'Fabbiani, Cristian';
        aux.precio := 4100000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DELANTERO;
        jugadores[26] := aux;

        aux.id := 27;
        aux.nombre := 'Gigliotti, Emanuel';
        aux.precio := 3500000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DELANTERO;
        jugadores[27] := aux;

        aux.id := 28;
        aux.nombre := 'Matos, Mauro';
        aux.precio := 4500000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DELANTERO;
        jugadores[28] := aux;

        aux.id := 29;
        aux.nombre := 'Ortega, Ariel';
        aux.precio := 6600000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DELANTERO;
        jugadores[29] := aux;

        aux.id := 30;
        aux.nombre := 'Rui, Henry';
        aux.precio := 400000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DELANTERO;
        jugadores[30] := aux;

        aux.id := 31;
        aux.nombre := 'Torassa, Agustin';
        aux.precio := 2500000;
        aux.equipo_real := 'All Boys';
        aux.posicion := DELANTERO;
        jugadores[31] := aux;

        aux.id := 32;
        aux.nombre := 'D Angelo, Sebastian';
        aux.precio := 300000;
        aux.equipo_real := 'Boca';
        aux.posicion := ARQUERO;
        jugadores[32] := aux;

        aux.id := 33;
        aux.nombre := 'Garcia, Javier';
        aux.precio := 4000000;
        aux.equipo_real := 'Boca';
        aux.posicion := ARQUERO;
        jugadores[33] := aux;

        aux.id := 34;
        aux.nombre := 'Lucchetti, Cristian';
        aux.precio := 5000000;
        aux.equipo_real := 'Boca';
        aux.posicion := ARQUERO;
        jugadores[34] := aux;

        aux.id := 35;
        aux.nombre := 'Scapparoni, Maximiliano';
        aux.precio := 300000;
        aux.equipo_real := 'Boca';
        aux.posicion := ARQUERO;
        jugadores[35] := aux;

        aux.id := 36;
        aux.nombre := 'Achucarro, David';
        aux.precio := 600000;
        aux.equipo_real := 'Boca';
        aux.posicion := DEFENSOR;
        jugadores[36] := aux;

        aux.id := 37;
        aux.nombre := 'Calvo, Jose Maria';
        aux.precio := 2300000;
        aux.equipo_real := 'Boca';
        aux.posicion := DEFENSOR;
        jugadores[37] := aux;

        aux.id := 38;
        aux.nombre := 'Caruzzo, Matias';
        aux.precio := 5200000;
        aux.equipo_real := 'Boca';
        aux.posicion := DEFENSOR;
        jugadores[38] := aux;

        aux.id := 39;
        aux.nombre := 'Cellay, Christian';
        aux.precio := 4900000;
        aux.equipo_real := 'Boca';
        aux.posicion := DEFENSOR;
        jugadores[39] := aux;

        aux.id := 40;
        aux.nombre := 'Insaurralde, Juan Manuel';
        aux.precio := 5300000;
        aux.equipo_real := 'Boca';
        aux.posicion := DEFENSOR;
        jugadores[40] := aux;

        aux.id := 41;
        aux.nombre := 'Marin, Leandro Lucas';
        aux.precio := 600000;
        aux.equipo_real := 'Boca';
        aux.posicion := DEFENSOR;
        jugadores[41] := aux;

        aux.id := 42;
        aux.nombre := 'Monzon, Luciano Fabian';
        aux.precio := 3500000;
        aux.equipo_real := 'Boca';
        aux.posicion := DEFENSOR;
        jugadores[42] := aux;

        aux.id := 43;
        aux.nombre := 'Rodriguez, Clemente';
        aux.precio := 5100000;
        aux.equipo_real := 'Boca';
        aux.posicion := DEFENSOR;
        jugadores[43] := aux;

        aux.id := 44;
        aux.nombre := 'Ruiz, Enzo Adrian';
        aux.precio := 600000;
        aux.equipo_real := 'Boca';
        aux.posicion := DEFENSOR;
        jugadores[44] := aux;

        aux.id := 45;
        aux.nombre := 'Sauro, Gaston';
        aux.precio := 1900000;
        aux.equipo_real := 'Boca';
        aux.posicion := DEFENSOR;
        jugadores[45] := aux;

        aux.id := 46;
        aux.nombre := 'Battaglia, Sebastian';
        aux.precio := 5700000;
        aux.equipo_real := 'Boca';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[46] := aux;

        aux.id := 47;
        aux.nombre := 'Chavez, Cristian';
        aux.precio := 4000000;
        aux.equipo_real := 'Boca';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[47] := aux;

        aux.id := 48;
        aux.nombre := 'Colazo, Nicolas';
        aux.precio := 2300000;
        aux.equipo_real := 'Boca';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[48] := aux;

        aux.id := 49;
        aux.nombre := 'Erbes, Cristian';
        aux.precio := 2700000;
        aux.equipo_real := 'Boca';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[49] := aux;

        aux.id := 50;
        aux.nombre := 'Erviti, Walter';
        aux.precio := 10100000;
        aux.equipo_real := 'Boca';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[50] := aux;

        aux.id := 51;
        aux.nombre := 'Tristelme, Juan Roman';
        aux.precio := 9500000;
        aux.equipo_real := 'Boca';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[51] := aux;

        aux.id := 52;
        aux.nombre := 'Rivero, Diego';
        aux.precio := 4500000;
        aux.equipo_real := 'Boca';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[52] := aux;

        aux.id := 53;
        aux.nombre := 'Sanchez Mino, Juan';
        aux.precio := 600000;
        aux.equipo_real := 'Boca';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[53] := aux;

        aux.id := 54;
        aux.nombre := 'Somoza, Leandro';
        aux.precio := 5000000;
        aux.equipo_real := 'Boca';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[54] := aux;

        aux.id := 55;
        aux.nombre := 'Acosta, Joel';
        aux.precio := 500000;
        aux.equipo_real := 'Boca';
        aux.posicion := DELANTERO;
        jugadores[55] := aux;

        aux.id := 56;
        aux.nombre := 'Araujo, Sergio';
        aux.precio := 2000000;
        aux.equipo_real := 'Boca';
        aux.posicion := DELANTERO;
        jugadores[56] := aux;

        aux.id := 57;
        aux.nombre := 'Gaona Lugo, Orlando';
        aux.precio := 1000000;
        aux.equipo_real := 'Boca';
        aux.posicion := DELANTERO;
        jugadores[57] := aux;

        aux.id := 58;
        aux.nombre := 'Imbert, Juan Martin';
        aux.precio := 300000;
        aux.equipo_real := 'Boca';
        aux.posicion := DELANTERO;
        jugadores[58] := aux;

        aux.id := 59;
        aux.nombre := 'Mouche, Pablo';
        aux.precio := 4900000;
        aux.equipo_real := 'Boca';
        aux.posicion := DELANTERO;
        jugadores[59] := aux;

        aux.id := 60;
        aux.nombre := 'Noir, Ricardo';
        aux.precio := 2500000;
        aux.equipo_real := 'Boca';
        aux.posicion := DELANTERO;
        jugadores[60] := aux;

        aux.id := 61;
        aux.nombre := 'Palermo, Martin';
        aux.precio := 12800000;
        aux.equipo_real := 'Boca';
        aux.posicion := DELANTERO;
        jugadores[61] := aux;

        aux.id := 62;
        aux.nombre := 'Paredes, Leandro';
        aux.precio := 500000;
        aux.equipo_real := 'Boca';
        aux.posicion := DELANTERO;
        jugadores[62] := aux;

        aux.id := 63;
        aux.nombre := 'Viatri, Lucas';
        aux.precio := 4800000;
        aux.equipo_real := 'Boca';
        aux.posicion := DELANTERO;
        jugadores[63] := aux;

        aux.id := 64;
        aux.nombre := 'Bologna, Enrique';
        aux.precio := 4300000;
        aux.equipo_real := 'Banfield';
        aux.posicion := ARQUERO;
        jugadores[64] := aux;

        aux.id := 65;
        aux.nombre := 'Grassi, Franco';
        aux.precio := 300000;
        aux.equipo_real := 'Banfield';
        aux.posicion := ARQUERO;
        jugadores[65] := aux;

        aux.id := 66;
        aux.nombre := 'Santillo, Pablo';
        aux.precio := 2500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := ARQUERO;
        jugadores[66] := aux;

        aux.id := 67;
        aux.nombre := 'Barraza, Julio';
        aux.precio := 2800000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DEFENSOR;
        jugadores[67] := aux;

        aux.id := 68;
        aux.nombre := 'Bustamante, Marcelo';
        aux.precio := 3500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DEFENSOR;
        jugadores[68] := aux;

        aux.id := 69;
        aux.nombre := 'Delfino, Alejandro';
        aux.precio := 900000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DEFENSOR;
        jugadores[69] := aux;

        aux.id := 70;
        aux.nombre := 'Devaca, Jose';
        aux.precio := 2300000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DEFENSOR;
        jugadores[70] := aux;

        aux.id := 71;
        aux.nombre := 'Dos Santos, Mauro';
        aux.precio := 3400000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DEFENSOR;
        jugadores[71] := aux;

        aux.id := 72;
        aux.nombre := 'Ladino, Santiago';
        aux.precio := 2800000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DEFENSOR;
        jugadores[72] := aux;

        aux.id := 73;
        aux.nombre := 'Lopez, Victor';
        aux.precio := 5000000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DEFENSOR;
        jugadores[73] := aux;

        aux.id := 74;
        aux.nombre := 'Segovia, Favio';
        aux.precio := 1800000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DEFENSOR;
        jugadores[74] := aux;

        aux.id := 75;
        aux.nombre := 'Tagliafico, Nicolas';
        aux.precio := 600000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DEFENSOR;
        jugadores[75] := aux;

        aux.id := 76;
        aux.nombre := 'Toledo, Gustavo';
        aux.precio := 1000000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DEFENSOR;
        jugadores[76] := aux;

        aux.id := 77;
        aux.nombre := 'Yeri, Nahuel';
        aux.precio := 700000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DEFENSOR;
        jugadores[77] := aux;

        aux.id := 78;
        aux.nombre := 'Barbaro, Alejandro';
        aux.precio := 300000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[78] := aux;

        aux.id := 79;
        aux.nombre := 'Bustos, Maximiliano';
        aux.precio := 3000000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[79] := aux;

        aux.id := 80;
        aux.nombre := 'Carrusca, Marcelo';
        aux.precio := 4500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[80] := aux;

        aux.id := 81;
        aux.nombre := 'De Souza, Diego';
        aux.precio := 3500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[81] := aux;

        aux.id := 82;
        aux.nombre := 'Gomez, Jonatan';
        aux.precio := 3500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[82] := aux;

        aux.id := 83;
        aux.nombre := 'Guillermo Rojas, Julian';
        aux.precio := 400000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[83] := aux;

        aux.id := 84;
        aux.nombre := 'Maciel, Gerardo';
        aux.precio := 300000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[84] := aux;

        aux.id := 85;
        aux.nombre := 'Pereyra, Hernan';
        aux.precio := 500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[85] := aux;

        aux.id := 86;
        aux.nombre := 'Pio, Emmanuel';
        aux.precio := 2500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[86] := aux;

        aux.id := 87;
        aux.nombre := 'Quinteros, Marcelo';
        aux.precio := 3900000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[87] := aux;

        aux.id := 88;
        aux.nombre := 'Romero, Sebastian Ariel';
        aux.precio := 4500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[88] := aux;

        aux.id := 89;
        aux.nombre := 'Rosada, Ariel';
        aux.precio := 4000000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[89] := aux;

        aux.id := 90;
        aux.nombre := 'Sardella, Federico';
        aux.precio := 1500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[90] := aux;

        aux.id := 91;
        aux.nombre := 'Achucarro, Jorge';
        aux.precio := 4400000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DELANTERO;
        jugadores[91] := aux;

        aux.id := 92;
        aux.nombre := 'Bauchet, Nicolas';
        aux.precio := 300000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DELANTERO;
        jugadores[92] := aux;

        aux.id := 93;
        aux.nombre := 'Ferreyra, Facundo';
        aux.precio := 1500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DELANTERO;
        jugadores[93] := aux;

        aux.id := 94;
        aux.nombre := 'Garcia, Cristian';
        aux.precio := 3200000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DELANTERO;
        jugadores[94] := aux;

        aux.id := 95;
        aux.nombre := 'Mendez, Gabriel';
        aux.precio := 3500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DELANTERO;
        jugadores[95] := aux;

        aux.id := 96;
        aux.nombre := 'Solignac, Luis';
        aux.precio := 2300000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DELANTERO;
        jugadores[96] := aux;

        aux.id := 97;
        aux.nombre := 'Zelaya, Emilio';
        aux.precio := 4500000;
        aux.equipo_real := 'Banfield';
        aux.posicion := DELANTERO;
        jugadores[97] := aux;

        aux.id := 98;
        aux.nombre := 'Carrera, Juan Ignacio';
        aux.precio := 2000000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := ARQUERO;
        jugadores[98] := aux;

        aux.id := 99;
        aux.nombre := 'Navarro, Nicolas';
        aux.precio := 4800000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := ARQUERO;
        jugadores[99] := aux;

        aux.id := 100;
        aux.nombre := 'Ojeda, Luis';
        aux.precio := 2500000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := ARQUERO;
        jugadores[100] := aux;

        aux.id := 101;
        aux.nombre := 'Berardo, Nicolas';
        aux.precio := 1600000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DEFENSOR;
        jugadores[101] := aux;

        aux.id := 102;
        aux.nombre := 'Escudero, Sergio';
        aux.precio := 3600000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DEFENSOR;
        jugadores[102] := aux;

        aux.id := 103;
        aux.nombre := 'Gentiletti, Santiago';
        aux.precio := 4300000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DEFENSOR;
        jugadores[103] := aux;

        aux.id := 104;
        aux.nombre := 'Pistone, Federico';
        aux.precio := 500000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DEFENSOR;
        jugadores[104] := aux;

        aux.id := 105;
        aux.nombre := 'Prosperi, Gonzalo';
        aux.precio := 4100000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DEFENSOR;
        jugadores[105] := aux;

        aux.id := 106;
        aux.nombre := 'Rodriguez, Lucas';
        aux.precio := 600000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DEFENSOR;
        jugadores[106] := aux;

        aux.id := 107;
        aux.nombre := 'Sabia, Juan';
        aux.precio := 3700000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DEFENSOR;
        jugadores[107] := aux;

        aux.id := 108;
        aux.nombre := 'Torren, Miguel';
        aux.precio := 2800000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DEFENSOR;
        jugadores[108] := aux;

        aux.id := 109;
        aux.nombre := 'Basualdo, German';
        aux.precio := 2200000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[109] := aux;

        aux.id := 110;
        aux.nombre := 'Bogado, Mauro';
        aux.precio := 2600000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[110] := aux;

        aux.id := 111;
        aux.nombre := 'Hernandez, Emilio';
        aux.precio := 3200000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[111] := aux;

        aux.id := 112;
        aux.nombre := 'Hernandez, Pablo';
        aux.precio := 2800000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[112] := aux;

        aux.id := 113;
        aux.nombre := 'Laba, Matias';
        aux.precio := 400000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[113] := aux;

        aux.id := 114;
        aux.nombre := 'Mercier, Juan Ignacio';
        aux.precio := 6800000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[114] := aux;

        aux.id := 115;
        aux.nombre := 'Ocampo, Dario';
        aux.precio := 2500000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[115] := aux;

        aux.id := 116;
        aux.nombre := 'Sanchez Prette, Christian';
        aux.precio := 4000000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[116] := aux;

        aux.id := 117;
        aux.nombre := 'Barrera, Leandro';
        aux.precio := 600000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DELANTERO;
        jugadores[117] := aux;

        aux.id := 118;
        aux.nombre := 'Blandi, Nicolas';
        aux.precio := 2500000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DELANTERO;
        jugadores[118] := aux;

        aux.id := 119;
        aux.nombre := 'Niell, Franco';
        aux.precio := 4800000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DELANTERO;
        jugadores[119] := aux;

        aux.id := 120;
        aux.nombre := 'Oberman, Gustavo';
        aux.precio := 3000000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DELANTERO;
        jugadores[120] := aux;

        aux.id := 121;
        aux.nombre := 'Rius, Ciro';
        aux.precio := 2500000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DELANTERO;
        jugadores[121] := aux;

        aux.id := 122;
        aux.nombre := 'Romero, Andres';
        aux.precio := 1500000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DELANTERO;
        jugadores[122] := aux;

        aux.id := 123;
        aux.nombre := 'Salcedo, Santiago';
        aux.precio := 5000000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DELANTERO;
        jugadores[123] := aux;

        aux.id := 124;
        aux.nombre := 'Vargas, Gonzalo';
        aux.precio := 4400000;
        aux.equipo_real := 'Argentinos';
        aux.posicion := DELANTERO;
        jugadores[124] := aux;

        aux.id := 125;
        aux.nombre := 'Burrai, Javier';
        aux.precio := 300000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := ARQUERO;
        jugadores[125] := aux;

        aux.id := 126;
        aux.nombre := 'Campestrini, Cristian';
        aux.precio := 5400000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := ARQUERO;
        jugadores[126] := aux;

        aux.id := 127;
        aux.nombre := 'Orcellet, Catriel';
        aux.precio := 1500000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := ARQUERO;
        jugadores[127] := aux;

        aux.id := 128;
        aux.nombre := 'Aguilar, Pablo Cesar';
        aux.precio := 4100000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DEFENSOR;
        jugadores[128] := aux;

        aux.id := 129;
        aux.nombre := 'Bogino, Ignacio';
        aux.precio := 2000000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DEFENSOR;
        jugadores[129] := aux;

        aux.id := 130;
        aux.nombre := 'Cepeda, Christian';
        aux.precio := 500000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DEFENSOR;
        jugadores[130] := aux;

        aux.id := 131;
        aux.nombre := 'Gonzalez, Adrian';
        aux.precio := 3500000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DEFENSOR;
        jugadores[131] := aux;

        aux.id := 132;
        aux.nombre := 'Krupoviesa, Juan';
        aux.precio := 3600000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DEFENSOR;
        jugadores[132] := aux;

        aux.id := 133;
        aux.nombre := 'Lopez, Lisandro';
        aux.precio := 4000000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DEFENSOR;
        jugadores[133] := aux;

        aux.id := 134;
        aux.nombre := 'Menendez, Gonzalo';
        aux.precio := 1000000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DEFENSOR;
        jugadores[134] := aux;

        aux.id := 135;
        aux.nombre := 'Nervo, Hugo Martin';
        aux.precio := 3000000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DEFENSOR;
        jugadores[135] := aux;

        aux.id := 136;
        aux.nombre := 'Perez, Damian Alfredo';
        aux.precio := 1600000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DEFENSOR;
        jugadores[136] := aux;

        aux.id := 137;
        aux.nombre := 'Aguirre, Marcos';
        aux.precio := 4200000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[137] := aux;

        aux.id := 138;
        aux.nombre := 'Alvarez, Cristian';
        aux.precio := 3800000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[138] := aux;

        aux.id := 139;
        aux.nombre := 'Caffa, Juan Pablo';
        aux.precio := 4100000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[139] := aux;

        aux.id := 140;
        aux.nombre := 'Choy Gonzalez, Gonzalo';
        aux.precio := 3500000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[140] := aux;

        aux.id := 141;
        aux.nombre := 'Esmerado, Gaston';
        aux.precio := 3000000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[141] := aux;

        aux.id := 142;
        aux.nombre := 'Marcone, Ivan';
        aux.precio := 2900000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[142] := aux;

        aux.id := 143;
        aux.nombre := 'Mosca, Claudio';
        aux.precio := 3500000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[143] := aux;

        aux.id := 144;
        aux.nombre := 'Ortiz, Jorge';
        aux.precio := 3900000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[144] := aux;

        aux.id := 145;
        aux.nombre := 'Sena, Sergio';
        aux.precio := 3900000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[145] := aux;

        aux.id := 146;
        aux.nombre := 'Sierra, Matias';
        aux.precio := 600000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[146] := aux;

        aux.id := 147;
        aux.nombre := 'Silva, Facundo';
        aux.precio := 1300000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[147] := aux;

        aux.id := 148;
        aux.nombre := 'Alustiza, Matias';
        aux.precio := 4000000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DELANTERO;
        jugadores[148] := aux;

        aux.id := 149;
        aux.nombre := 'Blanco, Gustavo';
        aux.precio := 300000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DELANTERO;
        jugadores[149] := aux;

        aux.id := 150;
        aux.nombre := 'Franzoia, Andres';
        aux.precio := 3800000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DELANTERO;
        jugadores[150] := aux;

        aux.id := 151;
        aux.nombre := 'Leguizamon, Luciano';
        aux.precio := 5400000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DELANTERO;
        jugadores[151] := aux;

        aux.id := 152;
        aux.nombre := 'Mendoza, Franco';
        aux.precio := 3600000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DELANTERO;
        jugadores[152] := aux;

        aux.id := 153;
        aux.nombre := 'Obolo, Mauro Ivan';
        aux.precio := 5300000;
        aux.equipo_real := 'Arsenal';
        aux.posicion := DELANTERO;
        jugadores[153] := aux;

        aux.id := 154;
        aux.nombre := 'Bailo, Andres';
        aux.precio := 600000;
        aux.equipo_real := 'Colon';
        aux.posicion := ARQUERO;
        jugadores[154] := aux;

        aux.id := 155;
        aux.nombre := 'Diaz, Marcos';
        aux.precio := 1300000;
        aux.equipo_real := 'Colon';
        aux.posicion := ARQUERO;
        jugadores[155] := aux;

        aux.id := 156;
        aux.nombre := 'Pozo, Diego';
        aux.precio := 5000000;
        aux.equipo_real := 'Colon';
        aux.posicion := ARQUERO;
        jugadores[156] := aux;

        aux.id := 157;
        aux.nombre := 'Arias, Mauricio';
        aux.precio := 300000;
        aux.equipo_real := 'Colon';
        aux.posicion := DEFENSOR;
        jugadores[157] := aux;

        aux.id := 158;
        aux.nombre := 'Caire, Maximiliano';
        aux.precio := 1700000;
        aux.equipo_real := 'Colon';
        aux.posicion := DEFENSOR;
        jugadores[158] := aux;

        aux.id := 159;
        aux.nombre := 'Candia, Salustiano';
        aux.precio := 2500000;
        aux.equipo_real := 'Colon';
        aux.posicion := DEFENSOR;
        jugadores[159] := aux;

        aux.id := 160;
        aux.nombre := 'Garce, Ariel';
        aux.precio := 4500000;
        aux.equipo_real := 'Colon';
        aux.posicion := DEFENSOR;
        jugadores[160] := aux;

        aux.id := 161;
        aux.nombre := 'Goux, Marcelo';
        aux.precio := 4100000;
        aux.equipo_real := 'Colon';
        aux.posicion := DEFENSOR;
        jugadores[161] := aux;

        aux.id := 162;
        aux.nombre := 'Mendoza, Humberto';
        aux.precio := 3600000;
        aux.equipo_real := 'Colon';
        aux.posicion := DEFENSOR;
        jugadores[162] := aux;

        aux.id := 163;
        aux.nombre := 'Quilez, Ismael';
        aux.precio := 3000000;
        aux.equipo_real := 'Colon';
        aux.posicion := DEFENSOR;
        jugadores[163] := aux;

        aux.id := 164;
        aux.nombre := 'Quiroga, Juan';
        aux.precio := 3200000;
        aux.equipo_real := 'Colon';
        aux.posicion := DEFENSOR;
        jugadores[164] := aux;

        aux.id := 165;
        aux.nombre := 'Raldes, Ronald';
        aux.precio := 3800000;
        aux.equipo_real := 'Colon';
        aux.posicion := DEFENSOR;
        jugadores[165] := aux;

        aux.id := 166;
        aux.nombre := 'Acosta, Lucas';
        aux.precio := 2800000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[166] := aux;

        aux.id := 167;
        aux.nombre := 'Bellone, Mauro';
        aux.precio := 2900000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[167] := aux;

        aux.id := 168;
        aux.nombre := 'Diaz, Damian';
        aux.precio := 4700000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[168] := aux;

        aux.id := 169;
        aux.nombre := 'Gomez, Ricardo';
        aux.precio := 3400000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[169] := aux;

        aux.id := 170;
        aux.nombre := 'Graciani, Gabriel';
        aux.precio := 500000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[170] := aux;

        aux.id := 171;
        aux.nombre := 'Ledesma, Cristian';
        aux.precio := 4100000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[171] := aux;

        aux.id := 172;
        aux.nombre := 'Moreno y Fabianesi, Ivan';
        aux.precio := 4900000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[172] := aux;

        aux.id := 173;
        aux.nombre := 'Mugni, Lucas';
        aux.precio := 500000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[173] := aux;

        aux.id := 174;
        aux.nombre := 'Prediger, Sebastian';
        aux.precio := 4400000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[174] := aux;

        aux.id := 175;
        aux.nombre := 'Ramirez, Alfredo';
        aux.precio := 3000000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[175] := aux;

        aux.id := 176;
        aux.nombre := 'Soto, Santiago';
        aux.precio := 1300000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[176] := aux;

        aux.id := 177;
        aux.nombre := 'Zaracho, Edgar';
        aux.precio := 2500000;
        aux.equipo_real := 'Colon';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[177] := aux;

        aux.id := 178;
        aux.nombre := 'Cano, German';
        aux.precio := 2200000;
        aux.equipo_real := 'Colon';
        aux.posicion := DELANTERO;
        jugadores[178] := aux;

        aux.id := 179;
        aux.nombre := 'Comachi, Martin';
        aux.precio := 300000;
        aux.equipo_real := 'Colon';
        aux.posicion := DELANTERO;
        jugadores[179] := aux;

        aux.id := 180;
        aux.nombre := 'Curuchet, Facundo';
        aux.precio := 900000;
        aux.equipo_real := 'Colon';
        aux.posicion := DELANTERO;
        jugadores[180] := aux;

        aux.id := 181;
        aux.nombre := 'Fuertes, Esteban';
        aux.precio := 8000000;
        aux.equipo_real := 'Colon';
        aux.posicion := DELANTERO;
        jugadores[181] := aux;

        aux.id := 182;
        aux.nombre := 'Higuain, Federico';
        aux.precio := 5300000;
        aux.equipo_real := 'Colon';
        aux.posicion := DELANTERO;
        jugadores[182] := aux;

        aux.id := 183;
        aux.nombre := 'Larrivey, Joaquin';
        aux.precio := 5100000;
        aux.equipo_real := 'Colon';
        aux.posicion := DELANTERO;
        jugadores[183] := aux;

        aux.id := 184;
        aux.nombre := 'Lesman, German';
        aux.precio := 900000;
        aux.equipo_real := 'Colon';
        aux.posicion := DELANTERO;
        jugadores[184] := aux;

        aux.id := 185;
        aux.nombre := 'Luque, Carlos';
        aux.precio := 300000;
        aux.equipo_real := 'Colon';
        aux.posicion := DELANTERO;
        jugadores[185] := aux;

        aux.id := 186;
        aux.nombre := 'Orion, Agustin';
        aux.precio := 5900000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := ARQUERO;
        jugadores[186] := aux;

        aux.id := 187;
        aux.nombre := 'Silva, Agustin';
        aux.precio := 300000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := ARQUERO;
        jugadores[187] := aux;

        aux.id := 188;
        aux.nombre := 'Taborda, Cesar';
        aux.precio := 2000000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := ARQUERO;
        jugadores[188] := aux;

        aux.id := 189;
        aux.nombre := 'Benitez, Nelson';
        aux.precio := 4000000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DEFENSOR;
        jugadores[189] := aux;

        aux.id := 190;
        aux.nombre := 'Bonomo, Luciano';
        aux.precio := 300000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DEFENSOR;
        jugadores[190] := aux;

        aux.id := 191;
        aux.nombre := 'Desabato, Leandro';
        aux.precio := 5600000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DEFENSOR;
        jugadores[191] := aux;

        aux.id := 192;
        aux.nombre := 'Fernandez, Federico';
        aux.precio := 4300000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DEFENSOR;
        jugadores[192] := aux;

        aux.id := 193;
        aux.nombre := 'Gissi, Dylan';
        aux.precio := 300000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DEFENSOR;
        jugadores[193] := aux;

        aux.id := 194;
        aux.nombre := 'Iberbia, Raul';
        aux.precio := 1900000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DEFENSOR;
        jugadores[194] := aux;

        aux.id := 195;
        aux.nombre := 'Mercado, Gabriel';
        aux.precio := 6000000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DEFENSOR;
        jugadores[195] := aux;

        aux.id := 196;
        aux.nombre := 'Re, German';
        aux.precio := 5000000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DEFENSOR;
        jugadores[196] := aux;

        aux.id := 197;
        aux.nombre := 'Roncaglia, Facundo';
        aux.precio := 4100000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DEFENSOR;
        jugadores[197] := aux;

        aux.id := 198;
        aux.nombre := 'Rosales, Pablo';
        aux.precio := 300000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DEFENSOR;
        jugadores[198] := aux;

        aux.id := 199;
        aux.nombre := 'Sarulyte, Matias';
        aux.precio := 800000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DEFENSOR;
        jugadores[199] := aux;

        aux.id := 200;
        aux.nombre := 'Auzqui, Diego';
        aux.precio := 500000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[200] := aux;

        aux.id := 201;
        aux.nombre := 'Barrientos, Pablo';
        aux.precio := 7900000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[201] := aux;

        aux.id := 202;
        aux.nombre := 'Benitez, Leandro Damian';
        aux.precio := 5300000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[202] := aux;

        aux.id := 203;
        aux.nombre := 'Brana, Rodrigo';
        aux.precio := 5200000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[203] := aux;

        aux.id := 204;
        aux.nombre := 'Hoyos, Michael';
        aux.precio := 2800000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[204] := aux;

        aux.id := 205;
        aux.nombre := 'Jara, Leonardo';
        aux.precio := 500000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[205] := aux;

        aux.id := 206;
        aux.nombre := 'Nunez, Maximiliano';
        aux.precio := 3300000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[206] := aux;

        aux.id := 207;
        aux.nombre := 'Penalba, Gabriel';
        aux.precio := 4000000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[207] := aux;

        aux.id := 208;
        aux.nombre := 'Perez, Enzo';
        aux.precio := 9500000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[208] := aux;

        aux.id := 209;
        aux.nombre := 'Sanchez, Matias';
        aux.precio := 3500000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[209] := aux;

        aux.id := 210;
        aux.nombre := 'Stefanatto, Dario';
        aux.precio := 2300000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[210] := aux;

        aux.id := 211;
        aux.nombre := 'Veron, Juan Sebastian';
        aux.precio := 14000000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[211] := aux;

        aux.id := 212;
        aux.nombre := 'Auzqui, Carlos';
        aux.precio := 900000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DELANTERO;
        jugadores[212] := aux;

        aux.id := 213;
        aux.nombre := 'Carrillo, Guido';
        aux.precio := 300000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DELANTERO;
        jugadores[213] := aux;

        aux.id := 214;
        aux.nombre := 'Fernandez, Gaston';
        aux.precio := 9200000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DELANTERO;
        jugadores[214] := aux;

        aux.id := 215;
        aux.nombre := 'Gonzalez, Leandro';
        aux.precio := 3500000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DELANTERO;
        jugadores[215] := aux;

        aux.id := 216;
        aux.nombre := 'Lopez, Hernan Rodrigo';
        aux.precio := 5500000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DELANTERO;
        jugadores[216] := aux;

        aux.id := 217;
        aux.nombre := 'Pereyra, Juan Pablo';
        aux.precio := 4000000;
        aux.equipo_real := 'Estudiantes';
        aux.posicion := DELANTERO;
        jugadores[217] := aux;

        aux.id := 218;
        aux.nombre := 'Bonnin, Yair';
        aux.precio := 300000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := ARQUERO;
        jugadores[218] := aux;

        aux.id := 219;
        aux.nombre := 'Monetti, Fernando';
        aux.precio := 1200000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := ARQUERO;
        jugadores[219] := aux;

        aux.id := 220;
        aux.nombre := 'Pellegrino, Fernando';
        aux.precio := 800000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := ARQUERO;
        jugadores[220] := aux;

        aux.id := 221;
        aux.nombre := 'Sessa, Gaston';
        aux.precio := 5000000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := ARQUERO;
        jugadores[221] := aux;

        aux.id := 222;
        aux.nombre := 'Aguero, Ariel';
        aux.precio := 4600000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[222] := aux;

        aux.id := 223;
        aux.nombre := 'Benitez, Oliver';
        aux.precio := 700000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[223] := aux;

        aux.id := 224;
        aux.nombre := 'Casco, Milton';
        aux.precio := 1900000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[224] := aux;

        aux.id := 225;
        aux.nombre := 'Fontanello, Pablo';
        aux.precio := 3800000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[225] := aux;

        aux.id := 226;
        aux.nombre := 'Iriarte, Hugo';
        aux.precio := 1800000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[226] := aux;

        aux.id := 227;
        aux.nombre := 'Magallan, Lisandro';
        aux.precio := 500000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[227] := aux;

        aux.id := 228;
        aux.nombre := 'Masuero, Abel';
        aux.precio := 2400000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[228] := aux;

        aux.id := 229;
        aux.nombre := 'Moreira, Ricardo';
        aux.precio := 2600000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[229] := aux;

        aux.id := 230;
        aux.nombre := 'Piarrou, Cristian';
        aux.precio := 2000000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[230] := aux;

        aux.id := 231;
        aux.nombre := 'Rieloff, Boris';
        aux.precio := 3000000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[231] := aux;

        aux.id := 232;
        aux.nombre := 'Sapetti, Leandro';
        aux.precio := 2500000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[232] := aux;

        aux.id := 233;
        aux.nombre := 'Soto, Gonzalo';
        aux.precio := 500000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DEFENSOR;
        jugadores[233] := aux;

        aux.id := 234;
        aux.nombre := 'Aued, Luciano';
        aux.precio := 4500000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[234] := aux;

        aux.id := 235;
        aux.nombre := 'Capurro, Alejandro';
        aux.precio := 3500000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[235] := aux;

        aux.id := 236;
        aux.nombre := 'Castro, Lucas';
        aux.precio := 3200000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[236] := aux;

        aux.id := 237;
        aux.nombre := 'Encina, Hernan';
        aux.precio := 4400000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[237] := aux;

        aux.id := 238;
        aux.nombre := 'Fernandez, Ignacio';
        aux.precio := 500000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[238] := aux;

        aux.id := 239;
        aux.nombre := 'Frezzotti, Alejandro';
        aux.precio := 2800000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[239] := aux;

        aux.id := 240;
        aux.nombre := 'Gonzalez, Cesar';
        aux.precio := 4100000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[240] := aux;

        aux.id := 241;
        aux.nombre := 'Mendez, Emiliano';
        aux.precio := 500000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[241] := aux;

        aux.id := 242;
        aux.nombre := 'Mussis, Franco';
        aux.precio := 300000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[242] := aux;

        aux.id := 243;
        aux.nombre := 'Rinaudo, Fabian';
        aux.precio := 4800000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[243] := aux;

        aux.id := 244;
        aux.nombre := 'Ruiz, Federico';
        aux.precio := 300000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[244] := aux;

        aux.id := 245;
        aux.nombre := 'B. Schelotto, Guillermo';
        aux.precio := 7000000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DELANTERO;
        jugadores[245] := aux;

        aux.id := 246;
        aux.nombre := 'Cordoba, Jorge';
        aux.precio := 3800000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DELANTERO;
        jugadores[246] := aux;

        aux.id := 247;
        aux.nombre := 'Curima, Agustin';
        aux.precio := 300000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DELANTERO;
        jugadores[247] := aux;

        aux.id := 248;
        aux.nombre := 'Graf, Claudio';
        aux.precio := 3800000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DELANTERO;
        jugadores[248] := aux;

        aux.id := 249;
        aux.nombre := 'Neira, Juan';
        aux.precio := 3700000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DELANTERO;
        jugadores[249] := aux;

        aux.id := 250;
        aux.nombre := 'Pacheco, German';
        aux.precio := 3000000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DELANTERO;
        jugadores[250] := aux;

        aux.id := 251;
        aux.nombre := 'Rojano, Antonio';
        aux.precio := 1000000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DELANTERO;
        jugadores[251] := aux;

        aux.id := 252;
        aux.nombre := 'Romea, Joaquin';
        aux.precio := 500000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DELANTERO;
        jugadores[252] := aux;

        aux.id := 253;
        aux.nombre := 'Vizcarra, Jose';
        aux.precio := 3500000;
        aux.equipo_real := 'Gimnasia';
        aux.posicion := DELANTERO;
        jugadores[253] := aux;

        aux.id := 254;
        aux.nombre := 'Ibanez, Nelson';
        aux.precio := 4400000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := ARQUERO;
        jugadores[254] := aux;

        aux.id := 255;
        aux.nombre := 'Moyano, Sebastian';
        aux.precio := 300000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := ARQUERO;
        jugadores[255] := aux;

        aux.id := 256;
        aux.nombre := 'Torrico, Sebastian';
        aux.precio := 4500000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := ARQUERO;
        jugadores[256] := aux;

        aux.id := 257;
        aux.nombre := 'Aguilera, Emanuel';
        aux.precio := 800000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DEFENSOR;
        jugadores[257] := aux;

        aux.id := 258;
        aux.nombre := 'Ceballos, Lucas';
        aux.precio := 1300000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DEFENSOR;
        jugadores[258] := aux;

        aux.id := 259;
        aux.nombre := 'Curbelo, Jorge';
        aux.precio := 4000000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DEFENSOR;
        jugadores[259] := aux;

        aux.id := 260;
        aux.nombre := 'Dutari, Francisco';
        aux.precio := 3500000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DEFENSOR;
        jugadores[260] := aux;

        aux.id := 261;
        aux.nombre := 'Faccioli, Emir';
        aux.precio := 3500000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DEFENSOR;
        jugadores[261] := aux;

        aux.id := 262;
        aux.nombre := 'Garcia, Zelmar';
        aux.precio := 2700000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DEFENSOR;
        jugadores[262] := aux;

        aux.id := 263;
        aux.nombre := 'Russo, Roberto';
        aux.precio := 2900000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DEFENSOR;
        jugadores[263] := aux;

        aux.id := 264;
        aux.nombre := 'Sanchez, Nicolas';
        aux.precio := 4200000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DEFENSOR;
        jugadores[264] := aux;

        aux.id := 265;
        aux.nombre := 'Sigali, Leonardo';
        aux.precio := 4800000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DEFENSOR;
        jugadores[265] := aux;

        aux.id := 266;
        aux.nombre := 'Voboril, German';
        aux.precio := 2500000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DEFENSOR;
        jugadores[266] := aux;

        aux.id := 267;
        aux.nombre := 'Camargo, Alejandro';
        aux.precio := 1600000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[267] := aux;

        aux.id := 268;
        aux.nombre := 'Damonte, Israel';
        aux.precio := 2500000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[268] := aux;

        aux.id := 269;
        aux.nombre := 'Donda, Mariano';
        aux.precio := 2800000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[269] := aux;

        aux.id := 270;
        aux.nombre := 'Moyano, Gabriel Oscar';
        aux.precio := 300000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[270] := aux;

        aux.id := 271;
        aux.nombre := 'Olmedo, Nicolas';
        aux.precio := 4800000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[271] := aux;

        aux.id := 272;
        aux.nombre := 'Rojas, Ariel';
        aux.precio := 4600000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[272] := aux;

        aux.id := 273;
        aux.nombre := 'Sanchez, Carlos';
        aux.precio := 5000000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[273] := aux;

        aux.id := 274;
        aux.nombre := 'Sanchez, Sergio';
        aux.precio := 1800000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[274] := aux;

        aux.id := 275;
        aux.nombre := 'Torres, Adrian';
        aux.precio := 900000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[275] := aux;

        aux.id := 276;
        aux.nombre := 'Villar, Diego';
        aux.precio := 3900000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[276] := aux;

        aux.id := 277;
        aux.nombre := 'Miranda, Pablo';
        aux.precio := 1500000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DELANTERO;
        jugadores[277] := aux;

        aux.id := 278;
        aux.nombre := 'Navarro, Alvaro';
        aux.precio := 3000000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DELANTERO;
        jugadores[278] := aux;

        aux.id := 279;
        aux.nombre := 'Nunez, Fabricio';
        aux.precio := 2900000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DELANTERO;
        jugadores[279] := aux;

        aux.id := 280;
        aux.nombre := 'Pinero da Silva, Jorge';
        aux.precio := 1500000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DELANTERO;
        jugadores[280] := aux;

        aux.id := 281;
        aux.nombre := 'Ramirez, Ruben';
        aux.precio := 6600000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DELANTERO;
        jugadores[281] := aux;

        aux.id := 282;
        aux.nombre := 'Salinas, Rodrigo';
        aux.precio := 3500000;
        aux.equipo_real := 'Godoy Cruz';
        aux.posicion := DELANTERO;
        jugadores[282] := aux;

        aux.id := 283;
        aux.nombre := 'Calvino, Lucas';
        aux.precio := 1000000;
        aux.equipo_real := 'Huracan';
        aux.posicion := ARQUERO;
        jugadores[283] := aux;

        aux.id := 284;
        aux.nombre := 'Daneri, Esteban';
        aux.precio := 300000;
        aux.equipo_real := 'Huracan';
        aux.posicion := ARQUERO;
        jugadores[284] := aux;

        aux.id := 285;
        aux.nombre := 'Monzon, Gaston';
        aux.precio := 4800000;
        aux.equipo_real := 'Huracan';
        aux.posicion := ARQUERO;
        jugadores[285] := aux;

        aux.id := 286;
        aux.nombre := 'Aguirre, Nicolas';
        aux.precio := 300000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[286] := aux;

        aux.id := 287;
        aux.nombre := 'Angeloff, David';
        aux.precio := 400000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[287] := aux;

        aux.id := 288;
        aux.nombre := 'Cura, Kevin';
        aux.precio := 1700000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[288] := aux;

        aux.id := 289;
        aux.nombre := 'Filippetto, Ezequiel';
        aux.precio := 2800000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[289] := aux;

        aux.id := 290;
        aux.nombre := 'Lemos, Rodrigo';
        aux.precio := 2000000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[290] := aux;

        aux.id := 291;
        aux.nombre := 'Lopez, Martin';
        aux.precio := 300000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[291] := aux;

        aux.id := 292;
        aux.nombre := 'Ospina, Luciano';
        aux.precio := 600000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[292] := aux;

        aux.id := 293;
        aux.nombre := 'Pena, Agustin';
        aux.precio := 1900000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[293] := aux;

        aux.id := 294;
        aux.nombre := 'Quintana, Carlos';
        aux.precio := 2900000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[294] := aux;

        aux.id := 295;
        aux.nombre := 'Quiroga, Facundo';
        aux.precio := 4000000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[295] := aux;

        aux.id := 296;
        aux.nombre := 'Vallejos, Nicolas';
        aux.precio := 800000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[296] := aux;

        aux.id := 297;
        aux.nombre := 'Villan, Leonardo';
        aux.precio := 1000000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DEFENSOR;
        jugadores[297] := aux;

        aux.id := 298;
        aux.nombre := 'Battaglia, Rodrigo';
        aux.precio := 3300000;
        aux.equipo_real := 'Huracan';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[298] := aux;

        aux.id := 299;
        aux.nombre := 'Britez Ojeda, Marcos';
        aux.precio := 2000000;
        aux.equipo_real := 'Huracan';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[299] := aux;

        aux.id := 300;
        aux.nombre := 'Machin, Gaston';
        aux.precio := 3500000;
        aux.equipo_real := 'Huracan';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[300] := aux;

        aux.id := 301;
        aux.nombre := 'Maidana, Cristian';
        aux.precio := 4000000;
        aux.equipo_real := 'Huracan';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[301] := aux;

        aux.id := 302;
        aux.nombre := 'Montiglio, Cesar';
        aux.precio := 3600000;
        aux.equipo_real := 'Huracan';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[302] := aux;

        aux.id := 303;
        aux.nombre := 'Morales, Angel';
        aux.precio := 4000000;
        aux.equipo_real := 'Huracan';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[303] := aux;

        aux.id := 304;
        aux.nombre := 'Pages, Fernando';
        aux.precio := 1000000;
        aux.equipo_real := 'Huracan';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[304] := aux;

        aux.id := 305;
        aux.nombre := 'Quintana, Alejandro';
        aux.precio := 900000;
        aux.equipo_real := 'Huracan';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[305] := aux;

        aux.id := 306;
        aux.nombre := 'Quiroga, Matias';
        aux.precio := 2800000;
        aux.equipo_real := 'Huracan';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[306] := aux;

        aux.id := 307;
        aux.nombre := 'Soplan, Dario';
        aux.precio := 1900000;
        aux.equipo_real := 'Huracan';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[307] := aux;

        aux.id := 308;
        aux.nombre := 'Velez, Leandro Nicolas';
        aux.precio := 400000;
        aux.equipo_real := 'Huracan';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[308] := aux;

        aux.id := 309;
        aux.nombre := 'Bottaro, Julian';
        aux.precio := 900000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DELANTERO;
        jugadores[309] := aux;

        aux.id := 310;
        aux.nombre := 'Campora, Javier';
        aux.precio := 4000000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DELANTERO;
        jugadores[310] := aux;

        aux.id := 311;
        aux.nombre := 'Guerra, Claudio';
        aux.precio := 3500000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DELANTERO;
        jugadores[311] := aux;

        aux.id := 312;
        aux.nombre := 'Lencina, Emiliano';
        aux.precio := 800000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DELANTERO;
        jugadores[312] := aux;

        aux.id := 313;
        aux.nombre := 'Nieto, Luciano';
        aux.precio := 2000000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DELANTERO;
        jugadores[313] := aux;

        aux.id := 314;
        aux.nombre := 'Oviedo, Nahuel';
        aux.precio := 400000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DELANTERO;
        jugadores[314] := aux;

        aux.id := 315;
        aux.nombre := 'Roffes, Guillermo';
        aux.precio := 900000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DELANTERO;
        jugadores[315] := aux;

        aux.id := 316;
        aux.nombre := 'Zarate, Rolando';
        aux.precio := 4000000;
        aux.equipo_real := 'Huracan';
        aux.posicion := DELANTERO;
        jugadores[316] := aux;

        aux.id := 317;
        aux.nombre := 'Assmann, Fabian';
        aux.precio := 4000000;
        aux.equipo_real := 'Independiente';
        aux.posicion := ARQUERO;
        jugadores[317] := aux;

        aux.id := 318;
        aux.nombre := 'Gabbarini, Adrian';
        aux.precio := 4500000;
        aux.equipo_real := 'Independiente';
        aux.posicion := ARQUERO;
        jugadores[318] := aux;

        aux.id := 319;
        aux.nombre := 'Navarro, Hilario';
        aux.precio := 6500000;
        aux.equipo_real := 'Independiente';
        aux.posicion := ARQUERO;
        jugadores[319] := aux;

        aux.id := 320;
        aux.nombre := 'Rodriguez, Diego';
        aux.precio := 600000;
        aux.equipo_real := 'Independiente';
        aux.posicion := ARQUERO;
        jugadores[320] := aux;

        aux.id := 321;
        aux.nombre := 'Baez, Cristian';
        aux.precio := 1200000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[321] := aux;

        aux.id := 322;
        aux.nombre := 'Barcia, Ignacio';
        aux.precio := 800000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[322] := aux;

        aux.id := 323;
        aux.nombre := 'Caceres, Samuel';
        aux.precio := 800000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[323] := aux;

        aux.id := 324;
        aux.nombre := 'Galeano, Leonel';
        aux.precio := 4300000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[324] := aux;

        aux.id := 325;
        aux.nombre := 'Kruspzky, Lucas';
        aux.precio := 800000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[325] := aux;

        aux.id := 326;
        aux.nombre := 'Mareque, Lucas';
        aux.precio := 4900000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[326] := aux;

        aux.id := 327;
        aux.nombre := 'Matheu, Carlos';
        aux.precio := 4900000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[327] := aux;

        aux.id := 328;
        aux.nombre := 'Tuzzio, Eduardo';
        aux.precio := 5000000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[328] := aux;

        aux.id := 329;
        aux.nombre := 'Valles, Gabriel';
        aux.precio := 3000000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[329] := aux;

        aux.id := 330;
        aux.nombre := 'Velazquez, Julian';
        aux.precio := 3900000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[330] := aux;

        aux.id := 331;
        aux.nombre := 'Velazquez, Maximiliano';
        aux.precio := 4000000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[331] := aux;

        aux.id := 332;
        aux.nombre := 'Velez, Jose';
        aux.precio := 2600000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DEFENSOR;
        jugadores[332] := aux;

        aux.id := 333;
        aux.nombre := 'Battion, Roberto';
        aux.precio := 4600000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[333] := aux;

        aux.id := 334;
        aux.nombre := 'Busse, Walter';
        aux.precio := 3900000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[334] := aux;

        aux.id := 335;
        aux.nombre := 'Cabrera, Nicolas';
        aux.precio := 4000000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[335] := aux;

        aux.id := 336;
        aux.nombre := 'Fredes, Hernan';
        aux.precio := 4000000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[336] := aux;

        aux.id := 337;
        aux.nombre := 'Godoy, Fernando';
        aux.precio := 2000000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[337] := aux;

        aux.id := 338;
        aux.nombre := 'Gracian, Leandro';
        aux.precio := 4300000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[338] := aux;

        aux.id := 339;
        aux.nombre := 'Mancuello, Federico';
        aux.precio := 3000000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[339] := aux;

        aux.id := 340;
        aux.nombre := 'Martinez, Nicolas';
        aux.precio := 2000000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[340] := aux;

        aux.id := 341;
        aux.nombre := 'Pellerano, Cristian';
        aux.precio := 3700000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[341] := aux;

        aux.id := 342;
        aux.nombre := 'Perez, Jorge Ivan';
        aux.precio := 800000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[342] := aux;

        aux.id := 343;
        aux.nombre := 'Rodriguez, Patricio';
        aux.precio := 3800000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[343] := aux;

        aux.id := 344;
        aux.nombre := 'Villafanez, Lucas';
        aux.precio := 1500000;
        aux.equipo_real := 'Independiente';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[344] := aux;

        aux.id := 345;
        aux.nombre := 'Castillo, Jairo';
        aux.precio := 4500000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DELANTERO;
        jugadores[345] := aux;

        aux.id := 346;
        aux.nombre := 'Defederico, Matias';
        aux.precio := 4900000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DELANTERO;
        jugadores[346] := aux;

        aux.id := 347;
        aux.nombre := 'Delmonte, Nicolas';
        aux.precio := 800000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DELANTERO;
        jugadores[347] := aux;

        aux.id := 348;
        aux.nombre := 'Nieva, Brian';
        aux.precio := 800000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DELANTERO;
        jugadores[348] := aux;

        aux.id := 349;
        aux.nombre := 'Nunez, Leonel';
        aux.precio := 5700000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DELANTERO;
        jugadores[349] := aux;

        aux.id := 350;
        aux.nombre := 'Parra, Facundo';
        aux.precio := 6000000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DELANTERO;
        jugadores[350] := aux;

        aux.id := 351;
        aux.nombre := 'Silvera, Andres';
        aux.precio := 8000000;
        aux.equipo_real := 'Independiente';
        aux.posicion := DELANTERO;
        jugadores[351] := aux;

        aux.id := 352;
        aux.nombre := 'Guzman, Nahuel';
        aux.precio := 1000000;
        aux.equipo_real := 'Newells';
        aux.posicion := ARQUERO;
        jugadores[352] := aux;

        aux.id := 353;
        aux.nombre := 'Hoyos, Lucas';
        aux.precio := 500000;
        aux.equipo_real := 'Newells';
        aux.posicion := ARQUERO;
        jugadores[353] := aux;

        aux.id := 354;
        aux.nombre := 'Peratta, Sebastian';
        aux.precio := 6200000;
        aux.equipo_real := 'Newells';
        aux.posicion := ARQUERO;
        jugadores[354] := aux;

        aux.id := 355;
        aux.nombre := 'Cichero, Gabriel';
        aux.precio := 3700000;
        aux.equipo_real := 'Newells';
        aux.posicion := DEFENSOR;
        jugadores[355] := aux;

        aux.id := 356;
        aux.nombre := 'Fideleff, Ignacio';
        aux.precio := 2400000;
        aux.equipo_real := 'Newells';
        aux.posicion := DEFENSOR;
        jugadores[356] := aux;

        aux.id := 357;
        aux.nombre := 'Fuentes, Fabricio';
        aux.precio := 4400000;
        aux.equipo_real := 'Newells';
        aux.posicion := DEFENSOR;
        jugadores[357] := aux;

        aux.id := 358;
        aux.nombre := 'Lema, Cristian';
        aux.precio := 1000000;
        aux.equipo_real := 'Newells';
        aux.posicion := DEFENSOR;
        jugadores[358] := aux;

        aux.id := 359;
        aux.nombre := 'Machuca, Alexis';
        aux.precio := 2400000;
        aux.equipo_real := 'Newells';
        aux.posicion := DEFENSOR;
        jugadores[359] := aux;

        aux.id := 360;
        aux.nombre := 'Scaglia, Mauricio';
        aux.precio := 300000;
        aux.equipo_real := 'Newells';
        aux.posicion := DEFENSOR;
        jugadores[360] := aux;

        aux.id := 361;
        aux.nombre := 'Schiavi, Rolando';
        aux.precio := 6000000;
        aux.equipo_real := 'Newells';
        aux.posicion := DEFENSOR;
        jugadores[361] := aux;

        aux.id := 362;
        aux.nombre := 'Valle, Jonathan';
        aux.precio := 300000;
        aux.equipo_real := 'Newells';
        aux.posicion := DEFENSOR;
        jugadores[362] := aux;

        aux.id := 363;
        aux.nombre := 'Vella, Luciano';
        aux.precio := 3700000;
        aux.equipo_real := 'Newells';
        aux.posicion := DEFENSOR;
        jugadores[363] := aux;

        aux.id := 364;
        aux.nombre := 'Bernardi, Lucas';
        aux.precio := 4500000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[364] := aux;

        aux.id := 365;
        aux.nombre := 'Camacho, Nestor';
        aux.precio := 3600000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[365] := aux;

        aux.id := 366;
        aux.nombre := 'Diaz, Cristian';
        aux.precio := 900000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[366] := aux;

        aux.id := 367;
        aux.nombre := 'Dolci, Pablo';
        aux.precio := 2200000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[367] := aux;

        aux.id := 368;
        aux.nombre := 'Estigarribia, Marcelo';
        aux.precio := 3800000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[368] := aux;

        aux.id := 369;
        aux.nombre := 'Faravelli, Lorenzo';
        aux.precio := 300000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[369] := aux;

        aux.id := 370;
        aux.nombre := 'Mateo, Diego';
        aux.precio := 3600000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[370] := aux;

        aux.id := 371;
        aux.nombre := 'Sperduti, Mauricio';
        aux.precio := 5000000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[371] := aux;

        aux.id := 372;
        aux.nombre := 'Tonso, Martin';
        aux.precio := 300000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[372] := aux;

        aux.id := 373;
        aux.nombre := 'Vangioni, Leonel';
        aux.precio := 3800000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[373] := aux;

        aux.id := 374;
        aux.nombre := 'Velazquez, Leandro';
        aux.precio := 3000000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[374] := aux;

        aux.id := 375;
        aux.nombre := 'Vieyra, Juan';
        aux.precio := 300000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[375] := aux;

        aux.id := 376;
        aux.nombre := 'Villalba, Hernan';
        aux.precio := 900000;
        aux.equipo_real := 'Newells';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[376] := aux;

        aux.id := 377;
        aux.nombre := 'Almiron, Sergio';
        aux.precio := 2800000;
        aux.equipo_real := 'Newells';
        aux.posicion := DELANTERO;
        jugadores[377] := aux;

        aux.id := 378;
        aux.nombre := 'Bieler, Claudio';
        aux.precio := 5000000;
        aux.equipo_real := 'Newells';
        aux.posicion := DELANTERO;
        jugadores[378] := aux;

        aux.id := 379;
        aux.nombre := 'Cobelli, Juan Manuel';
        aux.precio := 1200000;
        aux.equipo_real := 'Newells';
        aux.posicion := DELANTERO;
        jugadores[379] := aux;

        aux.id := 380;
        aux.nombre := 'Gonzalez, Cristian';
        aux.precio := 300000;
        aux.equipo_real := 'Newells';
        aux.posicion := DELANTERO;
        jugadores[380] := aux;

        aux.id := 381;
        aux.nombre := 'Rodriguez, Luis';
        aux.precio := 4400000;
        aux.equipo_real := 'Newells';
        aux.posicion := DELANTERO;
        jugadores[381] := aux;

        aux.id := 382;
        aux.nombre := 'Salvatierra, Daniel';
        aux.precio := 2000000;
        aux.equipo_real := 'Newells';
        aux.posicion := DELANTERO;
        jugadores[382] := aux;

        aux.id := 383;
        aux.nombre := 'Velasco, Maximiliano';
        aux.precio := 300000;
        aux.equipo_real := 'Newells';
        aux.posicion := DELANTERO;
        jugadores[383] := aux;

        aux.id := 384;
        aux.nombre := 'Andrada, Esteban';
        aux.precio := 600000;
        aux.equipo_real := 'Lanus';
        aux.posicion := ARQUERO;
        jugadores[384] := aux;

        aux.id := 385;
        aux.nombre := 'Caranta, Mauricio';
        aux.precio := 4700000;
        aux.equipo_real := 'Lanus';
        aux.posicion := ARQUERO;
        jugadores[385] := aux;

        aux.id := 386;
        aux.nombre := 'Marchesin, Agustin';
        aux.precio := 4000000;
        aux.equipo_real := 'Lanus';
        aux.posicion := ARQUERO;
        jugadores[386] := aux;

        aux.id := 387;
        aux.nombre := 'Pucheta, Jorge';
        aux.precio := 300000;
        aux.equipo_real := 'Lanus';
        aux.posicion := ARQUERO;
        jugadores[387] := aux;

        aux.id := 388;
        aux.nombre := 'Araujo, Carlos';
        aux.precio := 3000000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DEFENSOR;
        jugadores[388] := aux;

        aux.id := 389;
        aux.nombre := 'Arce, Carlos';
        aux.precio := 1900000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DEFENSOR;
        jugadores[389] := aux;

        aux.id := 390;
        aux.nombre := 'Balbi, Luciano';
        aux.precio := 1300000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DEFENSOR;
        jugadores[390] := aux;

        aux.id := 391;
        aux.nombre := 'Erramuspe, Rodrigo';
        aux.precio := 2600000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DEFENSOR;
        jugadores[391] := aux;

        aux.id := 392;
        aux.nombre := 'Goltz, Paolo Duval';
        aux.precio := 4500000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DEFENSOR;
        jugadores[392] := aux;

        aux.id := 393;
        aux.nombre := 'Hoyos, Santiago';
        aux.precio := 4000000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DEFENSOR;
        jugadores[393] := aux;

        aux.id := 394;
        aux.nombre := 'Izquierdoz, Carlos';
        aux.precio := 1300000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DEFENSOR;
        jugadores[394] := aux;

        aux.id := 395;
        aux.nombre := 'Lopes, Hernan';
        aux.precio := 600000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DEFENSOR;
        jugadores[395] := aux;

        aux.id := 396;
        aux.nombre := 'Lugo, Maximiliano';
        aux.precio := 2700000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DEFENSOR;
        jugadores[396] := aux;

        aux.id := 397;
        aux.nombre := 'Barrientos, Fernando';
        aux.precio := 300000;
        aux.equipo_real := 'Lanus';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[397] := aux;

        aux.id := 398;
        aux.nombre := 'Benitez, Oscar';
        aux.precio := 300000;
        aux.equipo_real := 'Lanus';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[398] := aux;

        aux.id := 399;
        aux.nombre := 'Camoranesi, Mauro';
        aux.precio := 5400000;
        aux.equipo_real := 'Lanus';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[399] := aux;

        aux.id := 400;
        aux.nombre := 'Carrasco, Javier';
        aux.precio := 1600000;
        aux.equipo_real := 'Lanus';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[400] := aux;

        aux.id := 401;
        aux.nombre := 'Gonzalez, Diego';
        aux.precio := 2500000;
        aux.equipo_real := 'Lanus';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[401] := aux;

        aux.id := 402;
        aux.nombre := 'Ledesma, Eduardo';
        aux.precio := 3700000;
        aux.equipo_real := 'Lanus';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[402] := aux;

        aux.id := 403;
        aux.nombre := 'Pelletieri, Agustin';
        aux.precio := 4800000;
        aux.equipo_real := 'Lanus';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[403] := aux;

        aux.id := 404;
        aux.nombre := 'Pizarro, Guido';
        aux.precio := 4300000;
        aux.equipo_real := 'Lanus';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[404] := aux;

        aux.id := 405;
        aux.nombre := 'Regueiro, Mario';
        aux.precio := 5500000;
        aux.equipo_real := 'Lanus';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[405] := aux;

        aux.id := 406;
        aux.nombre := 'Valeri, Diego';
        aux.precio := 7000000;
        aux.equipo_real := 'Lanus';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[406] := aux;

        aux.id := 407;
        aux.nombre := 'Carranza, Cesar';
        aux.precio := 4300000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DELANTERO;
        jugadores[407] := aux;

        aux.id := 408;
        aux.nombre := 'Castillejos, Gonzalo';
        aux.precio := 4500000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DELANTERO;
        jugadores[408] := aux;

        aux.id := 409;
        aux.nombre := 'Diaz, Leandro';
        aux.precio := 2200000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DELANTERO;
        jugadores[409] := aux;

        aux.id := 410;
        aux.nombre := 'Lagos, Diego';
        aux.precio := 3100000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DELANTERO;
        jugadores[410] := aux;

        aux.id := 411;
        aux.nombre := 'Romero, Silvio';
        aux.precio := 3000000;
        aux.equipo_real := 'Lanus';
        aux.posicion := DELANTERO;
        jugadores[411] := aux;

        aux.id := 412;
        aux.nombre := 'Arias, Gabriel';
        aux.precio := 600000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := ARQUERO;
        jugadores[412] := aux;

        aux.id := 413;
        aux.nombre := 'Ibanez, Matias';
        aux.precio := 1000000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := ARQUERO;
        jugadores[413] := aux;

        aux.id := 414;
        aux.nombre := 'Tombolini, Laureano';
        aux.precio := 3800000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := ARQUERO;
        jugadores[414] := aux;

        aux.id := 415;
        aux.nombre := 'Bianchi Arce, Nicolas';
        aux.precio := 3600000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DEFENSOR;
        jugadores[415] := aux;

        aux.id := 416;
        aux.nombre := 'Casais, Eduardo';
        aux.precio := 2900000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DEFENSOR;
        jugadores[416] := aux;

        aux.id := 417;
        aux.nombre := 'Diaz, Gabriel';
        aux.precio := 1100000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DEFENSOR;
        jugadores[417] := aux;

        aux.id := 418;
        aux.nombre := 'Dominguez, Federico';
        aux.precio := 3900000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DEFENSOR;
        jugadores[418] := aux;

        aux.id := 419;
        aux.nombre := 'Jerez, Pablo';
        aux.precio := 3000000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DEFENSOR;
        jugadores[419] := aux;

        aux.id := 420;
        aux.nombre := 'Mosset, Marcelo';
        aux.precio := 3000000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DEFENSOR;
        jugadores[420] := aux;

        aux.id := 421;
        aux.nombre := 'Reynoso, Diego';
        aux.precio := 2300000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DEFENSOR;
        jugadores[421] := aux;

        aux.id := 422;
        aux.nombre := 'Scheffer, Juan Pablo';
        aux.precio := 1700000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DEFENSOR;
        jugadores[422] := aux;

        aux.id := 423;
        aux.nombre := 'Tejera, Juan';
        aux.precio := 2300000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DEFENSOR;
        jugadores[423] := aux;

        aux.id := 424;
        aux.nombre := 'Villanueva, Cristian';
        aux.precio := 2000000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DEFENSOR;
        jugadores[424] := aux;

        aux.id := 425;
        aux.nombre := 'Aguirre, Martin';
        aux.precio := 2800000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[425] := aux;

        aux.id := 426;
        aux.nombre := 'Brum, Roberto';
        aux.precio := 3000000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[426] := aux;

        aux.id := 427;
        aux.nombre := 'Cobo, Juan Manuel';
        aux.precio := 3000000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[427] := aux;

        aux.id := 428;
        aux.nombre := 'Galvan, Diego';
        aux.precio := 3700000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[428] := aux;

        aux.id := 429;
        aux.nombre := 'Longo, Sebastian';
        aux.precio := 3000000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[429] := aux;

        aux.id := 430;
        aux.nombre := 'Mauri, Juan Alberto';
        aux.precio := 500000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[430] := aux;

        aux.id := 431;
        aux.nombre := 'Ricci, Marcelo';
        aux.precio := 1100000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[431] := aux;

        aux.id := 432;
        aux.nombre := 'Rolle, Martin';
        aux.precio := 2800000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[432] := aux;

        aux.id := 433;
        aux.nombre := 'Vega, David';
        aux.precio := 3200000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[433] := aux;

        aux.id := 434;
        aux.nombre := 'Bareiro, Nestor';
        aux.precio := 3500000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DELANTERO;
        jugadores[434] := aux;

        aux.id := 435;
        aux.nombre := 'Castillon, Federico';
        aux.precio := 2000000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DELANTERO;
        jugadores[435] := aux;

        aux.id := 436;
        aux.nombre := 'Furch, Julio';
        aux.precio := 1800000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DELANTERO;
        jugadores[436] := aux;

        aux.id := 437;
        aux.nombre := 'Litre, Marcos';
        aux.precio := 1000000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DELANTERO;
        jugadores[437] := aux;

        aux.id := 438;
        aux.nombre := 'Maggiolo, Ezequiel';
        aux.precio := 3800000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DELANTERO;
        jugadores[438] := aux;

        aux.id := 439;
        aux.nombre := 'Salom, Carlos';
        aux.precio := 2000000;
        aux.equipo_real := 'Olimpo';
        aux.posicion := DELANTERO;
        jugadores[439] := aux;

        aux.id := 440;
        aux.nombre := 'Galindez, Hernan';
        aux.precio := 3500000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := ARQUERO;
        jugadores[440] := aux;

        aux.id := 441;
        aux.nombre := 'Morales, Diego Hernan';
        aux.precio := 1000000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := ARQUERO;
        jugadores[441] := aux;

        aux.id := 442;
        aux.nombre := 'Tripodi, Emanuel';
        aux.precio := 3000000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := ARQUERO;
        jugadores[442] := aux;

        aux.id := 443;
        aux.nombre := 'Broggi, Ariel';
        aux.precio := 2600000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DEFENSOR;
        jugadores[443] := aux;

        aux.id := 444;
        aux.nombre := 'Corvalan, Claudio';
        aux.precio := 1800000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DEFENSOR;
        jugadores[444] := aux;

        aux.id := 445;
        aux.nombre := 'Di Gregorio, Matias';
        aux.precio := 1800000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DEFENSOR;
        jugadores[445] := aux;

        aux.id := 446;
        aux.nombre := 'Fontanini, Fabricio';
        aux.precio := 2900000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DEFENSOR;
        jugadores[446] := aux;

        aux.id := 447;
        aux.nombre := 'Gerlo, Danilo';
        aux.precio := 4000000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DEFENSOR;
        jugadores[447] := aux;

        aux.id := 448;
        aux.nombre := 'Gomez, Facundo';
        aux.precio := 800000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DEFENSOR;
        jugadores[448] := aux;

        aux.id := 449;
        aux.nombre := 'Grana, Hernan';
        aux.precio := 3500000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DEFENSOR;
        jugadores[449] := aux;

        aux.id := 450;
        aux.nombre := 'Martinez, Sebastian';
        aux.precio := 3000000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DEFENSOR;
        jugadores[450] := aux;

        aux.id := 451;
        aux.nombre := 'Quiles, Martin';
        aux.precio := 1800000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DEFENSOR;
        jugadores[451] := aux;

        aux.id := 452;
        aux.nombre := 'Caneo, Miguel';
        aux.precio := 3600000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[452] := aux;

        aux.id := 453;
        aux.nombre := 'Cerro, Francisco';
        aux.precio := 3000000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[453] := aux;

        aux.id := 454;
        aux.nombre := 'Coronel, Leandro';
        aux.precio := 2500000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[454] := aux;

        aux.id := 455;
        aux.nombre := 'Garnier, Pablo';
        aux.precio := 2500000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[455] := aux;

        aux.id := 456;
        aux.nombre := 'Gonzalez, Arnaldo';
        aux.precio := 300000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[456] := aux;

        aux.id := 457;
        aux.nombre := 'Kalinski, Enzo';
        aux.precio := 2800000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[457] := aux;

        aux.id := 458;
        aux.nombre := 'Nunez, Gervasio';
        aux.precio := 3500000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[458] := aux;

        aux.id := 459;
        aux.nombre := 'Raymonda, Santiago';
        aux.precio := 3800000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[459] := aux;

        aux.id := 460;
        aux.nombre := 'Varela, Gustavo';
        aux.precio := 2800000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[460] := aux;

        aux.id := 461;
        aux.nombre := 'Carrasco, Mauricio';
        aux.precio := 1500000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DELANTERO;
        jugadores[461] := aux;

        aux.id := 462;
        aux.nombre := 'Cauteruccio, Martin';
        aux.precio := 3000000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DELANTERO;
        jugadores[462] := aux;

        aux.id := 463;
        aux.nombre := 'Morales, Juan Jose';
        aux.precio := 4400000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DELANTERO;
        jugadores[463] := aux;

        aux.id := 464;
        aux.nombre := 'Narvay, Enrique';
        aux.precio := 1500000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DELANTERO;
        jugadores[464] := aux;

        aux.id := 465;
        aux.nombre := 'Romeo, Bernardo';
        aux.precio := 3000000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DELANTERO;
        jugadores[465] := aux;

        aux.id := 466;
        aux.nombre := 'Torres, Diego';
        aux.precio := 3100000;
        aux.equipo_real := 'Quilmes';
        aux.posicion := DELANTERO;
        jugadores[466] := aux;

        aux.id := 467;
        aux.nombre := 'Cafferatti, Leonel';
        aux.precio := 300000;
        aux.equipo_real := 'Racing';
        aux.posicion := ARQUERO;
        jugadores[467] := aux;

        aux.id := 468;
        aux.nombre := 'De Olivera, Jorge';
        aux.precio := 5000000;
        aux.equipo_real := 'Racing';
        aux.posicion := ARQUERO;
        jugadores[468] := aux;

        aux.id := 469;
        aux.nombre := 'Dobler, Mauro';
        aux.precio := 500000;
        aux.equipo_real := 'Racing';
        aux.posicion := ARQUERO;
        jugadores[469] := aux;

        aux.id := 470;
        aux.nombre := 'Fernandez, Roberto';
        aux.precio := 2800000;
        aux.equipo_real := 'Racing';
        aux.posicion := ARQUERO;
        jugadores[470] := aux;

        aux.id := 471;
        aux.nombre := 'Aveldano, Lucas';
        aux.precio := 3700000;
        aux.equipo_real := 'Racing';
        aux.posicion := DEFENSOR;
        jugadores[471] := aux;

        aux.id := 472;
        aux.nombre := 'Caceres, Marcos';
        aux.precio := 4000000;
        aux.equipo_real := 'Racing';
        aux.posicion := DEFENSOR;
        jugadores[472] := aux;

        aux.id := 473;
        aux.nombre := 'Cahais, Matias';
        aux.precio := 3800000;
        aux.equipo_real := 'Racing';
        aux.posicion := DEFENSOR;
        jugadores[473] := aux;

        aux.id := 474;
        aux.nombre := 'Colombatti, Gabriel';
        aux.precio := 300000;
        aux.equipo_real := 'Racing';
        aux.posicion := DEFENSOR;
        jugadores[474] := aux;

        aux.id := 475;
        aux.nombre := 'Garcia, Gonzalo';
        aux.precio := 2000000;
        aux.equipo_real := 'Racing';
        aux.posicion := DEFENSOR;
        jugadores[475] := aux;

        aux.id := 476;
        aux.nombre := 'Licht, Lucas';
        aux.precio := 4200000;
        aux.equipo_real := 'Racing';
        aux.posicion := DEFENSOR;
        jugadores[476] := aux;

        aux.id := 477;
        aux.nombre := 'Lluy, Braian';
        aux.precio := 2500000;
        aux.equipo_real := 'Racing';
        aux.posicion := DEFENSOR;
        jugadores[477] := aux;

        aux.id := 478;
        aux.nombre := 'Martinez, Matias';
        aux.precio := 4900000;
        aux.equipo_real := 'Racing';
        aux.posicion := DEFENSOR;
        jugadores[478] := aux;

        aux.id := 479;
        aux.nombre := 'Pillud, Ivan';
        aux.precio := 4800000;
        aux.equipo_real := 'Racing';
        aux.posicion := DEFENSOR;
        jugadores[479] := aux;

        aux.id := 480;
        aux.nombre := 'Sainz, Nicolas';
        aux.precio := 700000;
        aux.equipo_real := 'Racing';
        aux.posicion := DEFENSOR;
        jugadores[480] := aux;

        aux.id := 481;
        aux.nombre := 'Farina, Luis Carlos';
        aux.precio := 1300000;
        aux.equipo_real := 'Racing';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[481] := aux;

        aux.id := 482;
        aux.nombre := 'Mayorga, Sebastian';
        aux.precio := 1000000;
        aux.equipo_real := 'Racing';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[482] := aux;

        aux.id := 483;
        aux.nombre := 'Moreno, Giovanni';
        aux.precio := 10000000;
        aux.equipo_real := 'Racing';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[483] := aux;

        aux.id := 484;
        aux.nombre := 'Poclaba, Raul';
        aux.precio := 800000;
        aux.equipo_real := 'Racing';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[484] := aux;

        aux.id := 485;
        aux.nombre := 'Respuela, Juan Cruz';
        aux.precio := 600000;
        aux.equipo_real := 'Racing';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[485] := aux;

        aux.id := 486;
        aux.nombre := 'Toranzo, Patricio';
        aux.precio := 6900000;
        aux.equipo_real := 'Racing';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[486] := aux;

        aux.id := 487;
        aux.nombre := 'Yacob, Claudio';
        aux.precio := 4700000;
        aux.equipo_real := 'Racing';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[487] := aux;

        aux.id := 488;
        aux.nombre := 'Zuculini, Bruno';
        aux.precio := 1700000;
        aux.equipo_real := 'Racing';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[488] := aux;

        aux.id := 489;
        aux.nombre := 'Zuculini, Franco';
        aux.precio := 4200000;
        aux.equipo_real := 'Racing';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[489] := aux;

        aux.id := 490;
        aux.nombre := 'Colombini, Ignacio';
        aux.precio := 500000;
        aux.equipo_real := 'Racing';
        aux.posicion := DELANTERO;
        jugadores[490] := aux;

        aux.id := 491;
        aux.nombre := 'Gutierrez, Teofilo';
        aux.precio := 4300000;
        aux.equipo_real := 'Racing';
        aux.posicion := DELANTERO;
        jugadores[491] := aux;

        aux.id := 492;
        aux.nombre := 'Hauche, Gabriel';
        aux.precio := 7300000;
        aux.equipo_real := 'Racing';
        aux.posicion := DELANTERO;
        jugadores[492] := aux;

        aux.id := 493;
        aux.nombre := 'Luguercio, Pablo';
        aux.precio := 5200000;
        aux.equipo_real := 'Racing';
        aux.posicion := DELANTERO;
        jugadores[493] := aux;

        aux.id := 494;
        aux.nombre := 'Viola, Valentin';
        aux.precio := 700000;
        aux.equipo_real := 'Racing';
        aux.posicion := DELANTERO;
        jugadores[494] := aux;

        aux.id := 495;
        aux.nombre := 'Carrizo, Juan Pablo';
        aux.precio := 6400000;
        aux.equipo_real := 'River';
        aux.posicion := ARQUERO;
        jugadores[495] := aux;

        aux.id := 496;
        aux.nombre := 'Chichizola, Leandro';
        aux.precio := 1300000;
        aux.equipo_real := 'River';
        aux.posicion := ARQUERO;
        jugadores[496] := aux;

        aux.id := 497;
        aux.nombre := 'Vega, Mario Daniel';
        aux.precio := 3000000;
        aux.equipo_real := 'River';
        aux.posicion := ARQUERO;
        jugadores[497] := aux;

        aux.id := 498;
        aux.nombre := 'Arano, Carlos';
        aux.precio := 3800000;
        aux.equipo_real := 'River';
        aux.posicion := DEFENSOR;
        jugadores[498] := aux;

        aux.id := 499;
        aux.nombre := 'Coronel, Maximiliano';
        aux.precio := 1500000;
        aux.equipo_real := 'River';
        aux.posicion := DEFENSOR;
        jugadores[499] := aux;

        aux.id := 500;
        aux.nombre := 'Diaz, Juan Manuel';
        aux.precio := 3500000;
        aux.equipo_real := 'River';
        aux.posicion := DEFENSOR;
        jugadores[500] := aux;

        aux.id := 501;
        aux.nombre := 'Ferrari, Paulo';
        aux.precio := 5500000;
        aux.equipo_real := 'River';
        aux.posicion := DEFENSOR;
        jugadores[501] := aux;

        aux.id := 502;
        aux.nombre := 'Ferrero, Alexis';
        aux.precio := 4800000;
        aux.equipo_real := 'River';
        aux.posicion := DEFENSOR;
        jugadores[502] := aux;

        aux.id := 503;
        aux.nombre := 'Funes Mori, Jose Ramiro';
        aux.precio := 300000;
        aux.equipo_real := 'River';
        aux.posicion := DEFENSOR;
        jugadores[503] := aux;

        aux.id := 504;
        aux.nombre := 'Gonzalez Pirez, Leandro';
        aux.precio := 500000;
        aux.equipo_real := 'River';
        aux.posicion := DEFENSOR;
        jugadores[504] := aux;

        aux.id := 505;
        aux.nombre := 'Maidana, Jonatan';
        aux.precio := 4800000;
        aux.equipo_real := 'River';
        aux.posicion := DEFENSOR;
        jugadores[505] := aux;

        aux.id := 506;
        aux.nombre := 'Orban, Lucas';
        aux.precio := 1500000;
        aux.equipo_real := 'River';
        aux.posicion := DEFENSOR;
        jugadores[506] := aux;

        aux.id := 507;
        aux.nombre := 'Pezzella, German';
        aux.precio := 800000;
        aux.equipo_real := 'River';
        aux.posicion := DEFENSOR;
        jugadores[507] := aux;

        aux.id := 508;
        aux.nombre := 'Roman, Adalberto';
        aux.precio := 4800000;
        aux.equipo_real := 'River';
        aux.posicion := DEFENSOR;
        jugadores[508] := aux;

        aux.id := 509;
        aux.nombre := 'Abelairas, Matias';
        aux.precio := 3000000;
        aux.equipo_real := 'River';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[509] := aux;

        aux.id := 510;
        aux.nombre := 'Acevedo, Walter';
        aux.precio := 4600000;
        aux.equipo_real := 'River';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[510] := aux;

        aux.id := 511;
        aux.nombre := 'Affranchino, Facundo';
        aux.precio := 3400000;
        aux.equipo_real := 'River';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[511] := aux;

        aux.id := 512;
        aux.nombre := 'Almeyda, Matias';
        aux.precio := 5000000;
        aux.equipo_real := 'River';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[512] := aux;

        aux.id := 513;
        aux.nombre := 'Ballon, Josepmir';
        aux.precio := 2800000;
        aux.equipo_real := 'River';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[513] := aux;

        aux.id := 514;
        aux.nombre := 'Buonanotte, Diego';
        aux.precio := 6900000;
        aux.equipo_real := 'River';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[514] := aux;

        aux.id := 515;
        aux.nombre := 'Cirigliano, Ezequiel';
        aux.precio := 2200000;
        aux.equipo_real := 'River';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[515] := aux;

        aux.id := 516;
        aux.nombre := 'Diaz, Mauro';
        aux.precio := 3000000;
        aux.equipo_real := 'River';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[516] := aux;

        aux.id := 517;
        aux.nombre := 'Lamela, Erik';
        aux.precio := 4500000;
        aux.equipo_real := 'River';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[517] := aux;

        aux.id := 518;
        aux.nombre := 'Lanzini, Manuel';
        aux.precio := 2900000;
        aux.equipo_real := 'River';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[518] := aux;

        aux.id := 519;
        aux.nombre := 'Pereyra, Roberto';
        aux.precio := 3300000;
        aux.equipo_real := 'River';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[519] := aux;

        aux.id := 520;
        aux.nombre := 'Bordagaray, Fabian';
        aux.precio := 3700000;
        aux.equipo_real := 'River';
        aux.posicion := DELANTERO;
        jugadores[520] := aux;

        aux.id := 521;
        aux.nombre := 'Bou, Gustavo';
        aux.precio := 2000000;
        aux.equipo_real := 'River';
        aux.posicion := DELANTERO;
        jugadores[521] := aux;

        aux.id := 522;
        aux.nombre := 'Caruso, Leandro';
        aux.precio := 4000000;
        aux.equipo_real := 'River';
        aux.posicion := DELANTERO;
        jugadores[522] := aux;

        aux.id := 523;
        aux.nombre := 'Funes Mori, Rogelio';
        aux.precio := 4700000;
        aux.equipo_real := 'River';
        aux.posicion := DELANTERO;
        jugadores[523] := aux;

        aux.id := 524;
        aux.nombre := 'Pavone, Mariano';
        aux.precio := 8500000;
        aux.equipo_real := 'River';
        aux.posicion := DELANTERO;
        jugadores[524] := aux;

        aux.id := 525;
        aux.nombre := 'Villalva, Daniel';
        aux.precio := 3000000;
        aux.equipo_real := 'River';
        aux.posicion := DELANTERO;
        jugadores[525] := aux;

        aux.id := 526;
        aux.nombre := 'Acevedo, Gonzalo';
        aux.precio := 300000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := ARQUERO;
        jugadores[526] := aux;

        aux.id := 527;
        aux.nombre := 'Albil, Damian';
        aux.precio := 3800000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := ARQUERO;
        jugadores[527] := aux;

        aux.id := 528;
        aux.nombre := 'Migliore, Pablo';
        aux.precio := 5000000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := ARQUERO;
        jugadores[528] := aux;

        aux.id := 529;
        aux.nombre := 'Aguirre, Gaston';
        aux.precio := 4000000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[529] := aux;

        aux.id := 530;
        aux.nombre := 'Bottinelli, Jonathan';
        aux.precio := 4800000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[530] := aux;

        aux.id := 531;
        aux.nombre := 'Carmona, Giancarlo';
        aux.precio := 3200000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[531] := aux;

        aux.id := 532;
        aux.nombre := 'Chaparro, Lucas';
        aux.precio := 300000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[532] := aux;

        aux.id := 533;
        aux.nombre := 'Ferrari, Jonathan';
        aux.precio := 2900000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[533] := aux;

        aux.id := 534;
        aux.nombre := 'Herner, Diego';
        aux.precio := 3000000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[534] := aux;

        aux.id := 535;
        aux.nombre := 'Luna, Sebastian';
        aux.precio := 2800000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[535] := aux;

        aux.id := 536;
        aux.nombre := 'Meza, Fernando';
        aux.precio := 3000000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[536] := aux;

        aux.id := 537;
        aux.nombre := 'Palomino, Jose Luis';
        aux.precio := 1600000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[537] := aux;

        aux.id := 538;
        aux.nombre := 'Placente, Diego';
        aux.precio := 4600000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[538] := aux;

        aux.id := 539;
        aux.nombre := 'San Roman, Jose';
        aux.precio := 2900000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[539] := aux;

        aux.id := 540;
        aux.nombre := 'Torres, Aureliano';
        aux.precio := 4500000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[540] := aux;

        aux.id := 541;
        aux.nombre := 'Tula, Cristian';
        aux.precio := 5100000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DEFENSOR;
        jugadores[541] := aux;

        aux.id := 542;
        aux.nombre := 'Alvarado, Pablo';
        aux.precio := 1600000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[542] := aux;

        aux.id := 543;
        aux.nombre := 'Gimenez, Matias';
        aux.precio := 3800000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[543] := aux;

        aux.id := 544;
        aux.nombre := 'Gonzalez, Sebastian';
        aux.precio := 2500000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[544] := aux;

        aux.id := 545;
        aux.nombre := 'Gutierrez, Fernando';
        aux.precio := 600000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[545] := aux;

        aux.id := 546;
        aux.nombre := 'Navarro, Leandro';
        aux.precio := 300000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[546] := aux;

        aux.id := 547;
        aux.nombre := 'Ortigoza, Nestor';
        aux.precio := 8500000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[547] := aux;

        aux.id := 548;
        aux.nombre := 'Pereyra, Guillermo';
        aux.precio := 4000000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[548] := aux;

        aux.id := 549;
        aux.nombre := 'Reynoso, Salvador';
        aux.precio := 2000000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[549] := aux;

        aux.id := 550;
        aux.nombre := 'Romagnoli, Leandro';
        aux.precio := 6300000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[550] := aux;

        aux.id := 551;
        aux.nombre := 'Rusculleda, Sebastian';
        aux.precio := 3000000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[551] := aux;

        aux.id := 552;
        aux.nombre := 'Torres, Juan Manuel';
        aux.precio := 4200000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[552] := aux;

        aux.id := 553;
        aux.nombre := 'Benitez, Nahuel';
        aux.precio := 900000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DELANTERO;
        jugadores[553] := aux;

        aux.id := 554;
        aux.nombre := 'Ceballos, Lucas';
        aux.precio := 300000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DELANTERO;
        jugadores[554] := aux;

        aux.id := 555;
        aux.nombre := 'Menseguez, Juan Carlos';
        aux.precio := 5000000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DELANTERO;
        jugadores[555] := aux;

        aux.id := 556;
        aux.nombre := 'Pedrozo, Fabricio';
        aux.precio := 300000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DELANTERO;
        jugadores[556] := aux;

        aux.id := 557;
        aux.nombre := 'Salgueiro, Juan';
        aux.precio := 4700000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DELANTERO;
        jugadores[557] := aux;

        aux.id := 558;
        aux.nombre := 'Velazquez, Pablo';
        aux.precio := 4100000;
        aux.equipo_real := 'San Lorenzo';
        aux.posicion := DELANTERO;
        jugadores[558] := aux;

        aux.id := 559;
        aux.nombre := 'Ardente, Luis';
        aux.precio := 2500000;
        aux.equipo_real := 'Tigre';
        aux.posicion := ARQUERO;
        jugadores[559] := aux;

        aux.id := 560;
        aux.nombre := 'Islas, Daniel';
        aux.precio := 4700000;
        aux.equipo_real := 'Tigre';
        aux.posicion := ARQUERO;
        jugadores[560] := aux;

        aux.id := 561;
        aux.nombre := 'Vouilloud, German';
        aux.precio := 300000;
        aux.equipo_real := 'Tigre';
        aux.posicion := ARQUERO;
        jugadores[561] := aux;

        aux.id := 562;
        aux.nombre := 'Blengio, Juan Carlos';
        aux.precio := 3800000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DEFENSOR;
        jugadores[562] := aux;

        aux.id := 563;
        aux.nombre := 'Caceres, Pablo';
        aux.precio := 2800000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DEFENSOR;
        jugadores[563] := aux;

        aux.id := 564;
        aux.nombre := 'Echeverria, Mariano';
        aux.precio := 4500000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DEFENSOR;
        jugadores[564] := aux;

        aux.id := 565;
        aux.nombre := 'Laso, Joaquin';
        aux.precio := 300000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DEFENSOR;
        jugadores[565] := aux;

        aux.id := 566;
        aux.nombre := 'Mustafa, Daniel';
        aux.precio := 2500000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DEFENSOR;
        jugadores[566] := aux;

        aux.id := 567;
        aux.nombre := 'Paparatto, Norberto';
        aux.precio := 3000000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DEFENSOR;
        jugadores[567] := aux;

        aux.id := 568;
        aux.nombre := 'Pernia, Mariano';
        aux.precio := 3900000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DEFENSOR;
        jugadores[568] := aux;

        aux.id := 569;
        aux.nombre := 'Rodales, Andres';
        aux.precio := 2900000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DEFENSOR;
        jugadores[569] := aux;

        aux.id := 570;
        aux.nombre := 'Romero, Jonatan';
        aux.precio := 300000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DEFENSOR;
        jugadores[570] := aux;

        aux.id := 571;
        aux.nombre := 'Trombetta, Cristian';
        aux.precio := 2500000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DEFENSOR;
        jugadores[571] := aux;

        aux.id := 572;
        aux.nombre := 'Vera, Renzo';
        aux.precio := 2000000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DEFENSOR;
        jugadores[572] := aux;

        aux.id := 573;
        aux.nombre := 'Anzorena, Horacio';
        aux.precio := 1000000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[573] := aux;

        aux.id := 574;
        aux.nombre := 'Blanco, Jonathan';
        aux.precio := 2500000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[574] := aux;

        aux.id := 575;
        aux.nombre := 'Botta, Ruben';
        aux.precio := 3000000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[575] := aux;

        aux.id := 576;
        aux.nombre := 'Castano, Diego';
        aux.precio := 4800000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[576] := aux;

        aux.id := 577;
        aux.nombre := 'Diaz, Gaston';
        aux.precio := 1800000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[577] := aux;

        aux.id := 578;
        aux.nombre := 'Galmarini, Martin';
        aux.precio := 3300000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[578] := aux;

        aux.id := 579;
        aux.nombre := 'Gonzalez, Esteban';
        aux.precio := 3800000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[579] := aux;

        aux.id := 580;
        aux.nombre := 'Itabel, Kevin Fabian';
        aux.precio := 400000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[580] := aux;

        aux.id := 581;
        aux.nombre := 'Leone, Ramiro';
        aux.precio := 3900000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[581] := aux;

        aux.id := 582;
        aux.nombre := 'Martinez, Roman';
        aux.precio := 4900000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[582] := aux;

        aux.id := 583;
        aux.nombre := 'Menossi, Lucas';
        aux.precio := 300000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[583] := aux;

        aux.id := 584;
        aux.nombre := 'Morales, Diego';
        aux.precio := 3900000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[584] := aux;

        aux.id := 585;
        aux.nombre := 'Pasini, Mariano';
        aux.precio := 1500000;
        aux.equipo_real := 'Tigre';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[585] := aux;

        aux.id := 586;
        aux.nombre := 'Altobelli, Leonel';
        aux.precio := 2900000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DELANTERO;
        jugadores[586] := aux;

        aux.id := 587;
        aux.nombre := 'Gomez, Martin';
        aux.precio := 3400000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DELANTERO;
        jugadores[587] := aux;

        aux.id := 588;
        aux.nombre := 'Lanaro, Franco';
        aux.precio := 300000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DELANTERO;
        jugadores[588] := aux;

        aux.id := 589;
        aux.nombre := 'Millan, Nicolas';
        aux.precio := 1000000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DELANTERO;
        jugadores[589] := aux;

        aux.id := 590;
        aux.nombre := 'Simon, Lucas';
        aux.precio := 2800000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DELANTERO;
        jugadores[590] := aux;

        aux.id := 591;
        aux.nombre := 'Stracqualursi, Denis';
        aux.precio := 5700000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DELANTERO;
        jugadores[591] := aux;

        aux.id := 592;
        aux.nombre := 'Telechea, Fernando';
        aux.precio := 2900000;
        aux.equipo_real := 'Tigre';
        aux.posicion := DELANTERO;
        jugadores[592] := aux;

        aux.id := 593;
        aux.nombre := 'Aguerre, Alan';
        aux.precio := 300000;
        aux.equipo_real := 'Velez';
        aux.posicion := ARQUERO;
        jugadores[593] := aux;

        aux.id := 594;
        aux.nombre := 'Barovero, Marcelo';
        aux.precio := 5300000;
        aux.equipo_real := 'Velez';
        aux.posicion := ARQUERO;
        jugadores[594] := aux;

        aux.id := 595;
        aux.nombre := 'Montoya, Marcelo';
        aux.precio := 4500000;
        aux.equipo_real := 'Velez';
        aux.posicion := ARQUERO;
        jugadores[595] := aux;

        aux.id := 596;
        aux.nombre := 'Bittolo, Mariano';
        aux.precio := 900000;
        aux.equipo_real := 'Velez';
        aux.posicion := DEFENSOR;
        jugadores[596] := aux;

        aux.id := 597;
        aux.nombre := 'Diaz, Gaston';
        aux.precio := 3600000;
        aux.equipo_real := 'Velez';
        aux.posicion := DEFENSOR;
        jugadores[597] := aux;

        aux.id := 598;
        aux.nombre := 'Dominguez, Sebastian';
        aux.precio := 4800000;
        aux.equipo_real := 'Velez';
        aux.posicion := DEFENSOR;
        jugadores[598] := aux;

        aux.id := 599;
        aux.nombre := 'Olivera, Emanuel';
        aux.precio := 900000;
        aux.equipo_real := 'Velez';
        aux.posicion := DEFENSOR;
        jugadores[599] := aux;

        aux.id := 600;
        aux.nombre := 'Ortiz, Fernando';
        aux.precio := 4100000;
        aux.equipo_real := 'Velez';
        aux.posicion := DEFENSOR;
        jugadores[600] := aux;

        aux.id := 601;
        aux.nombre := 'Papa, Emiliano';
        aux.precio := 4900000;
        aux.equipo_real := 'Velez';
        aux.posicion := DEFENSOR;
        jugadores[601] := aux;

        aux.id := 602;
        aux.nombre := 'Peruzzi, Gino';
        aux.precio := 300000;
        aux.equipo_real := 'Velez';
        aux.posicion := DEFENSOR;
        jugadores[602] := aux;

        aux.id := 603;
        aux.nombre := 'Sills, Juan Ignacio';
        aux.precio := 300000;
        aux.equipo_real := 'Velez';
        aux.posicion := DEFENSOR;
        jugadores[603] := aux;

        aux.id := 604;
        aux.nombre := 'Tobio, Fernando';
        aux.precio := 3400000;
        aux.equipo_real := 'Velez';
        aux.posicion := DEFENSOR;
        jugadores[604] := aux;

        aux.id := 605;
        aux.nombre := 'Alvarez, Ricardo';
        aux.precio := 2900000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[605] := aux;

        aux.id := 606;
        aux.nombre := 'Bella, Ivan';
        aux.precio := 3000000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[606] := aux;

        aux.id := 607;
        aux.nombre := 'Canteros, Hector';
        aux.precio := 1900000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[607] := aux;

        aux.id := 608;
        aux.nombre := 'Cubero, Fabian';
        aux.precio := 4400000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[608] := aux;

        aux.id := 609;
        aux.nombre := 'Desabato, Leandro Luis';
        aux.precio := 400000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[609] := aux;

        aux.id := 610;
        aux.nombre := 'Fernandez, Augusto';
        aux.precio := 4800000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[610] := aux;

        aux.id := 611;
        aux.nombre := 'Freire, Federico';
        aux.precio := 300000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[611] := aux;

        aux.id := 612;
        aux.nombre := 'Gonzalez, Juan Nicolas';
        aux.precio := 300000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[612] := aux;

        aux.id := 613;
        aux.nombre := 'Moralez, Maximiliano';
        aux.precio := 11500000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[613] := aux;

        aux.id := 614;
        aux.nombre := 'Ramirez, David';
        aux.precio := 8600000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[614] := aux;

        aux.id := 615;
        aux.nombre := 'Razzotti, Franco';
        aux.precio := 4000000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[615] := aux;

        aux.id := 616;
        aux.nombre := 'Zapata, Victor';
        aux.precio := 5800000;
        aux.equipo_real := 'Velez';
        aux.posicion := MEDIOCAMPISTA;
        jugadores[616] := aux;

        aux.id := 617;
        aux.nombre := 'Correa, Jorge Ivan';
        aux.precio := 300000;
        aux.equipo_real := 'Velez';
        aux.posicion := DELANTERO;
        jugadores[617] := aux;

        aux.id := 618;
        aux.nombre := 'Ferreira, Brian';
        aux.precio := 300000;
        aux.equipo_real := 'Velez';
        aux.posicion := DELANTERO;
        jugadores[618] := aux;

        aux.id := 619;
        aux.nombre := 'Franco, Guillermo';
        aux.precio := 4900000;
        aux.equipo_real := 'Velez';
        aux.posicion := DELANTERO;
        jugadores[619] := aux;

        aux.id := 620;
        aux.nombre := 'Giusti, Jorge';
        aux.precio := 300000;
        aux.equipo_real := 'Velez';
        aux.posicion := DELANTERO;
        jugadores[620] := aux;

        aux.id := 621;
        aux.nombre := 'Martinez, Juan Manuel';
        aux.precio := 12000000;
        aux.equipo_real := 'Velez';
        aux.posicion := DELANTERO;
        jugadores[621] := aux;

        aux.id := 622;
        aux.nombre := 'Rescaldani, Ezequiel';
        aux.precio := 500000;
        aux.equipo_real := 'Velez';
        aux.posicion := DELANTERO;
        jugadores[622] := aux;

        aux.id := 623;
        aux.nombre := 'Silva, Santiago';
        aux.precio := 1230000;
        aux.equipo_real := 'Velez';
        aux.posicion := DELANTERO;
        jugadores[623] := aux;

        jugador := jugadores[id];
    end;

    function jugoPartido(id:integer): boolean;
    var
        jugador : tJugador;
        prob : real;
    begin
        obtenerJugador(id, jugador);
        prob := random();
        if (prob > jugador.precio/16000000) then
            jugoPartido := False
        else
            jugoPartido := True;
    end;

    function max(a,b : integer): integer;
    begin
        if a > b then
            max := a
        else
            max := b;
    end;

    function obtenerPuntajeJugador(id: integer): shortint;
    var
        puntaje : shortint;
        prob : real;
    begin
        prob := random();
        if prob < 0.05 then {El 5% da numeros entre -5 y 3}
            puntaje := random(4) -5
        else if prob < 0.15 then {El 10% da numeros entre 4 y 5 }
            {puntaje := random(4, 6)}
            puntaje := 4 + random(2)
        else if prob < 0.45 then {El 30% da numeros entre 6 y 7 }
            puntaje := 6 + random(2)
        else if prob < 0.65 then {El 20% da numeros entre 8 y 10 }
            puntaje := 8 + random(3)
        else if prob < 0.80 then {El 15% da numeros entre 11 y 13}
            puntaje := 11 + random(13)
        else if prob < 0.90 then {El 10% da numeros entre 14 y 21}
            puntaje := 14 + random(8)
        else if prob < 0.95 then {El 5% da numeros entre 22 y 25}
            puntaje := 22 + random(4)
        else {El 5% restante da numeros entre 25 y 35 }
            puntaje := 25 + random(11);

        if id = 51 then
            puntaje := -15
        else if (31 < id) and (id < 64) then
            puntaje := puntaje - 5
        else if (466 < id) and (id < 495) then
            puntaje := puntaje - 8
        else if (494 < id) and (id < 526) then
            puntaje := max(puntaje + 5, 14);
        obtenerPuntajeJugador := puntaje;
    end;

begin
    randomize();
end.
